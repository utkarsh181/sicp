;; All people who are big-shot in division ?DIVISION.  A pearson is big-shot if
;; he works in a division but does not have a supervisor who works in that
;; division.
(rule (big-shot ?division)
      (and (job ?name-1 (?division . ?rest-1))
	   (supervisor ?name-2 ?name-1)
	   (not (job ?name-2 (?division . ?rest-2)))))
