;;; Program to fix evaluation of quoted lazy list.

;; NOTE 2021-09-18: Warning! Don't evaluate this.  This is not Scheme, but
;; variant of scheme developed in "leval.scm".
(define (lcons (x lazy) (y lazy)) (lambda (m) (m x y)))
(define (lcar (z lazy)) (z (lambda (p q) p)))
(define (lcdr (z lazy)) (z (lambda (p q) q)))

(define (make-quoted exp)
  (list 'quoted
	(if (not (pair? exp))
	    exp
	    (lcons (car exp) (cdr exp)))))
