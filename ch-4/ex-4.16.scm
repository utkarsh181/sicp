;; Lookup value of variable VAR in ENV.
(define (lookup-variable-value var env)
  (cond
   ((eq? env empty-frame)
    (error "Unbound variable -- LOOKUP-VARIABLE-VALUE!" var))
   ((assoc var (first-frame env))
    => (lambda (binding)
	 (define var (car binding))
	 (define val (cdr binding))
	 (if (not (eq? val '*unassigned*))
	     val
	     (error "Unassigned variable -- LOOKUP-VARIABLE-VALUE"
		    var))))
   (else
    (lookup-variable-value var (rest-frame env)))))

(define (scan-out-defines proc-body)
  ;; Append VAR-VALUE pair to let expression EXP.
  (define (let-append var value exp)
    (if (null? exp)
	(make-let (list (list var (make-quoted '*unassigned*)))
		  (make-set! (make-quoted var) value))
	(append
	 (list 'let
	       (cons (list var (make-quoted '*unassigned*)) (cadr exp)))
	(list (make-set! (make-quoted var) value))
	(cddr exp))))

  ;; Transform define expression EXP with variables VARS and values VALUES into
  ;; a let expression.
  (define (define->let vars values exp)
    (if (or (null? vars) (null? values))
	exp
	(define->let
	 (cdr vars)
	 (cdr values)
	 (let-append (car vars) (car values) exp))))

  ;; Scan procedure body BODY for `defines' and accumulate them in VARS, VALUES
  ;; and FINAL-BODY.
  (define (scan body vars values final-body)
    (if (null? body)
	(list vars values final-body)
	(let ((first (car body))
	      (rest (cdr body)))
	  (if (not (definition? (car body)))
	      (scan rest vars values (cons first final-body))
	      (scan rest
		    (cons (definition-variable first) vars)
		    (cons (definition-value first) values)
		    final-body)))))

  (let* ((tmp-list (scan proc-body '() '() '()))
	(vars (car tmp-list))
	(values (cadr tmp-list))
	(final-body (caddr tmp-list)))
    (if (and (null? vars) (null? values))
	final-body
	(append (define->let vars values '()) final-body))))
