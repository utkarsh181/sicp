;; Return N'th element Fibonacci sequence using Y-operator technique.
(define (fib n)
  ((lambda (fib) (fib fib n))
   (lambda (ft k)
     (cond
      ((= k 0) 0)
      ((= k 1) 1)
      (else (+ (ft ft (- k 1)) (ft ft (- k 2))))))))

;; TODO 2021-09-14: Understand what this procedure is computing.
(define (f x)
  ((lambda (even? odd?)
     (even? even? odd? x))
   (lambda (ev? od? n)
     (if (= n 0)
	 #t
         (od? ev? od? (- n 1))))
   (lambda (ev? od? n)
     (if (= n 0)
         #f
         (ev? ev? od? (- n 1))))))
