;; Inference of relation ?REL between great-grand-son GGS and great-grand-father
;; GGF.
(rule ((great . ?rel) ?ggs ?ggf)
      (and (last-pair ?rel (grandson))
	   (?rel ?f ?ggf)
	   (son ?ggs ?f)))
