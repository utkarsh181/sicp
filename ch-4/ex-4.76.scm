;; A more mathematical approach to `and' for Query system.  Also see "query.scm".

;; By Eli: <https://eli.thegreenplace.net/2008/02/09/sicp-sections-442-444/>, I
;; must admit that I didn't tried my best for this one.

;; TODO 2021-09-30: Test it.

;; Merge frame FRAME1 and FRAME2.
(define (merge-frame frame1 frame2)
  (define (iter bindings result)
    (if (null? bindings)
	result
	(let* ((binding (car bindings))
	       (var (bindings-var bindings))
	       (value (binding-value bindings))
	       (extension (extend-if-possible var value frame2)))
	  (if (eq? extension 'failed)
	      'failed
	      (iter (cdr bindings) extension)))))

  (iter frame1 frame2))

;; Merge stream of frames STREAM1 and STREAM2.
(define (merge-stream-frame stream1 stream2)
  (stream-flatmap
   (lambda (frame1)
     (stream-filter
      (lambda (x) (not (eq? x 'failed)))
      (stream-map (lambda (frame2) (merge-stream frame1 frame2)) stream2)))
   stream1))

(define (conjoin conjuncts frame-stream)
  (if (empty-conjunction? conjuncts)
      frame-stream
      (merge-stream-frame
       (qeval (first-conjuct conjuncts frame-stream))
       (conjoin (rest-conjucts conjucts) frame-stream))))
