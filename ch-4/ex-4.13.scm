;;; Prototype for unbinding variable.  Also see "beval.scm".

(define (unbind-variable! var env)
  (set-car! (cdr env)
	    (assoc-remove! (first-frame env) var)))


;; `Unbind!' was give by Oavis at:
;; <https://www.inchmeal.io/sicp/ch-4/ex-4.15.html>

;; Remove the binding of symbol expression EXP from exvironment ENV.
(define (eval-unbind! exp env)
  ;; TODO 2021-09-12: Can we complain about false input?
  (unbind-variable! (car (operands exp)) env))
