;; Can person P1 replace P2.
(rule (can-replace ?p1 ?p2)
      (and (job ?p1 ?j1)
	   (job ?p2 ?j2)
	   (not (same ?p1 ?p2))
	   ;; By Oavis: <https://www.inchmeal.io/sicp/ch-4/ex-4.57>.  Earlier I
	   ;; was using a much less elegant version.
	   (or (same ?j1 ?j2)
	       (can-do-job ?j1 ?j2))))
