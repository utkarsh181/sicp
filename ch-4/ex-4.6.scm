;;; Prototype for `let'.  Also see "beval.scm".

(define (make-let var-list body)
  (list 'let var-list body))

;; Syntax for let: (let ((<var_1> <exp_1>) ... (<var_n> <exp_n>)) <body>)

;; Return body of let expression EXP.
(define (let-body exp) (cddr exp))

;; Return list of form: (<variables> <expression>) in let expression EXP.
(define (let-varlist exp)
  (cond
   ((or (null? (cadr exp)) (null? (car (cadr exp))))
    (error "Syntax error -- LET-VARLIST" exp))
   (else
    ;; REVIEW 2021-09-11: Is there a way to use single `map'?
    (list (map car (cadr exp))
	  (map cadr (cadr exp))))))

(define (let? exp) (tagged-list? exp 'let))

;; By Eli: <https://eli.thegreenplace.net/2007/12/06/sicp-sections-411-412>,
;; earlier I was using a hand-made iterative process.
(define (let->combination exp)
  (let* ((var-list (let-varlist exp))
	 (var (car var-list))
	 (exp (cadr var-list)))
    (cons (make-lambda var (let-body exp)) exp)))

(define (eval-let exp env)
  (beval-eval (let->combination exp) env))
