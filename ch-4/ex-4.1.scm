;;; Prototype for left-to-rignt and right-to-left evaluation.  Also see
;;; "beval.scm".

;; Left to right evaluation

(define (list-of-values exps env)
  (if (no-operands? exps)
      '()
      ;; NOTE 2021-09-09: Explicit use of `left' and 'right' denote order of
      ;; evaluation.
      (let ((left (ueval (first-operand exps) env))
	    (right (list-of-values (rest-operands exps) exps)))
	(cons left right))))

;; Right to left evaluation

(define (list-of-values-2 exps env)
  (if (no-operands? exps)
      '()
      ;; NOTE 2021-09-09: Explicit use of `left' and 'right' denote order of
      ;; evaluation.
      (let ((right (list-of-values-2 (rest-operands exps) exps))
	    (left (ueval (first-operand exps) env)))
	(cons left right))))
