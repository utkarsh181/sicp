;; Is ?S grandson of grandfather ?G.
(rule (grandson ?gs ?gf)
      (son ?gs ?f)
      (son ?f ?gf))

;; Is ?S son of mother ?M.
(rule (son ?s ?m)
      (wife ?w ?m)
      (son ?s ?w))
