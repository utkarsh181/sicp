(use-modules (sicp ch-4 ambeval))

;; NOTE 2021-09-19: Don't evaluate! This is a subset of Scheme implemented in a
;; non-deterministic (or ambiguous) evaluator in "ambeval.scm".

;; Check requirements of expression P begin "true".
(define (require p) (if (not p) (amb)))

;; Return ambiguously an element of list ITEMS.
(define (an-element-of items)
  (require (not (null? items)))
  (amb (car items) (an-element-of (cdr items))))

;; Return ambiguously an integer starting from N.
(define (an-integer-starting-from n)
  (amb n (an-integer-starting-from (1+ n))))

;; Return an integer between LOWER and UPPER bound.
(define (an-integer-between lower upper)
  (let ((l (1+ lower)))
    (require (< l upper))
    (amb l (an-integer-between (1+ l) upper))))

;; FIXME 2021-09-19: Why it's not working with `let*'?
(define (a-pythagorean-triple-between low high)
  (let ((i (an-integer-between low high)))
    (let ((j (an-integer-between i high)))
      (let ((k (an-integer-between j high)))
	(require (= (+ (* i i) (* j j)) (* k k)))
	(list i j k)))))
