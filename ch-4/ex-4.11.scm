;;; Prototype for alist as enviornment data structure.  Also see "beval.scm".

;;; Constructors

(define (make-environment frame)
  (cons '*env* frame))

(define (make-frame variables values)
  (map cons variables values))

;;; Selectors

(define (first-frame env)
  (if (eq? (car env) '*env*)
      (cadr env)
      (car env)))

(define (rest-frame env)
  (if (eq? (car env) '*env*)
      (cddr env)
      (cdr env)))

;;; Mutators

(define (set-variable-value! var val env)
  (cond
   ((eq? env empty-frame)
    (error "Unbound variable -- SET-VARIABLE-VALUE!" var))
   ((assoc var (first-frame env))
    => (lambda (binding) (set-cdr! binding val)))
   (else
    (set-variable-value! var val env))))

(define (define-variable! var val env)
  (set-car! (cdr env)
	    (assoc-set! (first-frame env) var val)))

;;; General Procedure

(define (extend-environment variables values base-env)
  (make-environment
    (cons (make-frame variables values) (cdr base-env))))

(define (lookup-variable-value var env)
  (cond
   ((eq? env empty-frame)
    (error "Unbound variable -- LOOKUP-VARIABLE-VALUE!" var))
   ((assoc var (first-frame env)) => cdr)
   (else
    (lookup-variable-value var (rest-frame env)))))

(define empty-frame (make-frame '() '()))

(define empty-environment (make-environment empty-frame))
