#+title: SICP Exercise 4.28
#+author: Utkarsh Singh

One of the simplistic example I can think, and is also part of test
suite is: =((lambda (x y) (+ x y)) 15 5)=

Trace of behaviour under discussion:

#+begin_src scheme
trace: (leval-eval ((lambda (x y) (+ x y)) 15 5) (*env* ((w evaluated-thunk 10) (id procedure # …) …)))
trace: |  (self-evaluating? ((lambda (x y) (+ x y)) 15 5))
trace: |  |  (boolean? ((lambda (x y) (+ x y)) 15 5))
trace: |  |  #f
trace: |  |  (number? ((lambda (x y) (+ x y)) 15 5))
trace: |  |  #f
trace: |  #f
trace: |  (variable? ((lambda (x y) (+ x y)) 15 5))
trace: |  #f
trace: |  (operator ((lambda (x y) (+ x y)) 15 5))
trace: |  (lambda (x y) (+ x y))
trace: |  (get eval (lambda (x y) (+ x y)))
trace: |  (hash-ref #<hash-table 7f317438d7c0 14/31> (eval (lambda (x y) (+ x y))) #f)
trace: |  #f
trace: |  (application? ((lambda (x y) (+ x y)) 15 5))
trace: |  #t
trace: |  (operator ((lambda (x y) (+ x y)) 15 5))
trace: |  (lambda (x y) (+ x y))
trace: |  (actual-value (lambda (x y) (+ x y)) (*env* ((w evaluated-thunk 10) (id procedure (x) …) …)))
...
#+end_src
