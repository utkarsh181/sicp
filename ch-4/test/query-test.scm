;;; Test suite for Query system

(use-modules (srfi srfi-64)
	     (srfi srfi-41)
	     (sicp ch-4 stream)
	     (sicp ch-4 query))

(define (interpret exp)
  (let ((q (query-syntax-process exp)))
    (car
     (stream->list
      (stream-map
       (lambda (frame)
	 (instantiate q frame (lambda (v f) (contract-question-mark v))))
       (qeval q (stream-singleton '())))))))

(test-begin "test-assert!")
(interpret '(assert! (hello query system!)))
(test-equal '(hello query system!) (interpret '(hello ?name system!)))

(interpret '(assert! (rule (append-to-form () ?y ?y))))
(interpret '(assert! (rule (append-to-form (?u . ?v) ?y (?u . ?z))
			   (append-to-form ?v ?y ?z))))

(test-equal '(append-to-form (1) (2 3) (1 2 3))
  (interpret '(append-to-form ?x (2 3) (1 2 3))))

(test-equal '(append-to-form (1 2) (3) (1 2 3))
  (interpret '(append-to-form (1 2) ?y (1 2 3))))
(test-end "test-assert!")
