;;; query.scm --- Query System

;;; Commentary:

;; This program is a simple yet powerful query system built on top of concepts
;; of Logic Programming described in SICP section 4.4.  It's evaluator defines a
;; special form `assert!' (syntax defined below), which is used to add an
;; assertions to the database.
;;
;; A query is just an Lisp expression, that logically searches for assertions in
;; the database.  In a query, a special token `?' (question mark) represents a
;; variable.  For example, the following query search's for all son's of person
;; named "Ram": (son ?x Ram)
;;
;; This special token is not just useful to represent the unknown (or variable)
;; but is also used to formulate compund queries.  For discussion on compund
;; queries, see aforementioned textbook reference.

;;; Code:

(define-module (sicp ch-4 query)
  #:use-module (sicp ch-4 table)
  #:use-module (srfi srfi-41)
  #:use-module (sicp ch-4 stream)
  #:export (execute
	    qeval
	    query-syntax-process
	    contract-question-mark
	    instantiate
	    query-driver-loop
	    initialize-database))


;; Counter used in renaming variables in `rule' expression.
(define *rule-counter* 0)

;; Streams of assertion in the database.
(define *assertions* stream-null)

;; Streams of `rule' expression in the database.
(define *rules* stream-null)

(define input-prompt ";;; Query input:")

(define output-prompt ";;; Query results:")

;;; Constructors

;; Bind variable VARIABLE to value VALUE.
(define (make-binding variable value)
  (cons variable value))

;; Generate a new variable VAR with id RULE-APPLICATION-ID.
(define (make-new-variable var rule-application-id)
  (cons '? (cons rule-application-id (cdr var))))

;; Return a new application ID.
(define (new-rule-application-id)
  (set! *rule-counter* (+ 1 *rule-counter*))
  *rule-counter*)

;;; Selectors

;; Return variable name for binding BINDING.
(define (binding-variable binding)
  (car binding))

;; Return value for binding BINDING.
(define (binding-value binding)
  (cdr binding))

;; Return binding for variable VARIABLE in frame FRAME.
(define (binding-in-frame variable frame)
  (assoc variable frame))

(define (type exp)
  (if (pair? exp)
      (car exp)
      (error 'type "Unknown expression TYPE" exp)))

(define (contents exp)
  (if (pair? exp)
      (cdr exp)
      (error 'contents "Unknown expression CONTENTS" exp)))

(define (assertion-body exp)
  (car exp))

(define (first-conjunct exps) (car exps))

(define (rest-conjuncts exps) (cdr exps))

(define (first-disjunct exps) (car exps))

(define (rest-disjuncts exps) (cdr exps))

(define (negated-query exps) (car exps))

(define (unique-query exp) (car exp))

(define (predicate exps) (car exps))

(define (args exps) (cdr exps))

(define (conclusion rule) (cadr rule))

(define (rule-body rule)
  (if (null? (cddr rule))
      '(always-true)
      (caddr rule)))

(define (get-all-assertions) *assertions*)

(define (stream-get key1 key2)
  (let ((s (get key1 key2)))
    (if s s stream-null)))

(define (get-indexed-assertions pattern)
  (stream-get (index-key-of pattern) 'stream-assertion))

;; Return stream of all assertion in the database matching PATTERN.
(define (fetch-assertions pattern)
  (if (use-index? pattern)
      (get-indexed-assertions pattern)
      (get-all-assertions)))

(define (get-all-rules) *rules*)

(define (get-indexed-rules pattern)
  (stream-append
   (stream-get (index-key-of pattern) 'stream-rule)
   ;; Get stream of all rules which starts with a variable.
   (stream-get '? 'stream-rule)))

(define (fetch-rules pattern frame)
  (if (use-index? pattern)
      (get-indexed-rules pattern)
      (get-all-rules)))

;; Return index of pattern PAT.
(define (index-key-of pat)
  (let ((key (car pat)))
    ;; Use `?' as index of pattern starting with a variable.
    (if (var? key) '? key)))

;;; Predicates

;; Is expression EXP tagged TAG?
(define (tagged-list? exp tag)
  (if (pair? exp)
      (eq? (car exp) tag)
      #f))

(define (assertion? exp)
  (eq? (type exp) 'assert!))

(define (empty-conjunction? exps) (null? exps))

(define (empty-disjunction? exps) (null? exps))

(define (rule? statement)
  (tagged-list? statement 'rule))

(define (var? exp)
  (tagged-list? exp '?))

(define (constant-symbol? exp) (symbol? exp))

(define (indexable? pat)
  (or (constant-symbol? (car pat))
      (var? (car pat))))

(define (use-index? pat)
  (constant-symbol? (car pat)))

(define (depends-on? exp var frame)
  (define (tree-walk e)
    (cond
     ((var? e)
      (if (equal? var e)
          #t
          (let ((b (binding-in-frame e frame)))
            (if b (tree-walk (binding-value b)) #f))))
     ((pair? e)
      (or (tree-walk (car e)) (tree-walk (cdr e))))
     (else #f)))
  (tree-walk exp))

;;; Mutators

(define (store-assertion-in-index assertion)
  (when (indexable? assertion)
    (let* ((key (index-key-of assertion))
	   (current-stream-assertion (stream-get key 'stream-assertion)))
      (put key
           'stream-assertion
           (stream-cons assertion current-stream-assertion)))))

(define (assertion-add! assertion)
  (store-assertion-in-index assertion)
  (let ((old-assertions *assertions*))
    (set! *assertions*
          (stream-cons assertion old-assertions))
    'ok))

(define (store-rule-in-index rule)
  (let ((pattern (conclusion rule)))
    (when (indexable? pattern)
      (let* ((key (index-key-of pattern))
	     (current-stream-rule (stream-get key 'stream-rule)))
        (put key
             'stream-rule
             (stream-cons rule current-stream-rule))))))

(define (rule-add! rule)
  (store-rule-in-index rule)
  (let ((old-rules *rules*))
    (set! *rules* (stream-cons rule old-rules))
    'ok))

(define (add-rule-or-assertion! assertion)
  (if (rule? assertion)
      (rule-add! assertion)
      (assertion-add! assertion)))

;;; General Procedures

(define (extend variable value frame)
  (cons (make-binding variable value) frame))

(define (expand-question-mark symbol)
  (let ((chars (symbol->string symbol)))
    (if (string=? (substring chars 0 1) "?")
        (list '?
              (string->symbol
               (substring chars 1 (string-length chars))))
        symbol)))

(define (map-over-symbols proc exp)
  (cond
   ((pair? exp)
    (cons (map-over-symbols proc (car exp))
          (map-over-symbols proc (cdr exp))))
   ((symbol? exp) (proc exp))
   (else exp)))

(define (query-syntax-process exp)
  (map-over-symbols expand-question-mark exp))

(define (contract-question-mark variable)
  (string->symbol
   (string-append
    "?"
    (if (number? (cadr variable))
	(string-append (symbol->string (caddr variable))
		       "-"
		       (number->string (cadr variable)))
	(symbol->string (cadr variable))))))

;; Extend frame FRAME if variable VAR with data DAT is consistent with binding
;; already present in the frame.
(define (extend-if-consistent var dat frame)
  (let ((binding (binding-in-frame var frame)))
    (if binding
        (pattern-match (binding-value binding) dat frame)
        (extend var dat frame))))

;; Match pattern PAT against data DAT in frame FRAME.
(define (pattern-match pat dat frame)
  (cond
   ((eq? frame 'failed) 'failed)
   ((equal? pat dat) frame)
   ((var? pat) (extend-if-consistent pat dat frame))
   ((and (pair? pat) (pair? dat))
    (pattern-match
     (cdr pat)
     (cdr dat)
     (pattern-match (car pat) (car dat) frame)))
   (else 'failed)))

;; Return a stream of extended frame by checking assertion ASSERTION against
;; query pattern QUERY-PAT in frame QUERY-FRAME.
(define (check-an-assertion assertion query-pat query-frame)
  (let ((match-result
         (pattern-match query-pat assertion query-frame)))
    (if (eq? match-result 'failed)
        stream-null
        (stream-singleton match-result))))

;; Return a stream of frames by extending frame FRAME by a database match of
;; pattern PATTERN.
(define (find-assertions pattern frame)
  (stream-simple-flatmap (lambda (datum)
			   (check-an-assertion datum pattern frame))
			 (fetch-assertions pattern)))

;; Rename variables in rule RULE.
(define (rename-variables-in rule)
  (let ((rule-application-id (new-rule-application-id)))
    (define (tree-walk exp)
      (cond
       ((var? exp)
        (make-new-variable exp rule-application-id))
       ((pair? exp)
        (cons (tree-walk (car exp)) (tree-walk (cdr exp))))
       (else exp)))

    (tree-walk rule)))

(define (extend-if-possible var val frame)
  (let ((binding (binding-in-frame var frame)))
    (cond
     (binding
      (unify-match (binding-value binding) val frame))
     ((var? val)
      (let ((binding (binding-in-frame val frame)))
        (if binding
            (unify-match
             var (binding-value binding) frame)
            (extend var val frame))))
     ((depends-on? val var frame)
      'failed)
     (else (extend var val frame)))))

;; Return an extended frame by unification of pattern P1 and P2 in frame FRAME.
(define (unify-match p1 p2 frame)
  (cond
   ((eq? frame 'failed) 'failed)
   ;; Avoid matching same patterns
   ((equal? p1 p2) frame)
   ((var? p1) (extend-if-possible p1 p2 frame))
   ;; This make it different from `pattern-match'.
   ((var? p2) (extend-if-possible p2 p1 frame))
   ((and (pair? p1) (pair? p2))
    (unify-match (cdr p1)
                 (cdr p2)
                 (unify-match (car p1) (car p2) frame)))
   (else 'failed)))

(define (apply-a-rule rule query-pattern query-frame)
  (let* ((clean-rule (rename-variables-in rule))
	 (unify-result
	  (unify-match query-pattern (conclusion clean-rule) query-frame)))
    (if (eq? unify-result 'failed)
        stream-null
        (qeval (rule-body clean-rule) (stream-singleton unify-result)))))

(define (apply-rules pattern frame)
  (stream-flatmap (lambda (rule)
                    (apply-a-rule rule pattern frame))
                  (fetch-rules pattern frame)))

;;; Apply

(define (execute exp)
  "Execute expression EXP."
  (apply (eval (predicate exp) (interaction-environment))
         (args exp)))

;;; Eval

;; NOTE 2021-09-29: Unlike SICP, language developed here has a built in special
;; form `assert!', as it will help other modules to directly manipulate database
;; with `qeval' procedure.

;; Evaluate assertion expression ASSERTION.
(define (qeval-assert! assertion stream-frame)
  (add-rule-or-assertion! (assertion-body assertion))
  stream-frame)

;; Return stream formed by matching each query pattern QUERTY-PATTERN in streams
;; of frame STREAM-FRAME.
(define (simple-query query-pattern stream-frame)
  (stream-flatmap
   (lambda (frame)
     (stream-append-delayed
      (find-assertions query-pattern frame)
      (delay (apply-rules query-pattern frame))))
   stream-frame))

;; Return a stream formed by evaluating conjugations in CONJUNCTS in stream of
;; frames STREAM-FRAME.
(define (conjoin conjuncts stream-frame)
  (if (empty-conjunction? conjuncts)
      stream-frame
      (conjoin (rest-conjuncts conjuncts)
               (qeval (first-conjunct conjuncts) stream-frame))))

;; Return a stream formed by evaluating dis-conjugation in DISJUNCTS in stream
;; of frames STREAM-FRAME.
(define (disjoin disjuncts stream-frame)
  (if (empty-disjunction? disjuncts)
      stream-null
      (interleave-delayed
       (qeval (first-disjunct disjuncts) stream-frame)
       (delay (disjoin (rest-disjuncts disjuncts) stream-frame)))))

;; Return a stream formed by negation of query expression OPERANDS in stream of
;; frames STREAM-FRAME.
(define (negate operands stream-frame)
  (stream-simple-flatmap
   (lambda (frame)
     (if (stream-null? (qeval (negated-query operands)
			      (stream-singleton frame)))
         (stream-singleton frame)
         stream-null))
   stream-frame))

;; Return a stream formed by evaluating Lisp-like query expression call CALL in
;; stream of frames STREAM-FRAME.
(define (lisp-value call stream-frame)
  (stream-simple-flatmap
   (lambda (frame)
     (if (execute
	  (instantiate
	      call
	      frame
	    (lambda (v f) (error 'lisp-value "Unknown pattern var -- LISP-VALUE" v))))
         (stream-singleton frame)
         stream-null))
   stream-frame))

(define (uniquely-asserted operands stream-frame)
  (stream-simple-flatmap
   (lambda (frame)
     (let ((result (qeval (unique-query operands) (stream-singleton frame))))
       (if (stream-equal-length? result 1)
	   result
           stream-null)))
   stream-frame))

(define (always-true ignore stream-frame)
  stream-frame)

(define (install-qeval-package)
  (put 'assert! 'qeval qeval-assert!)
  (put 'and 'qeval conjoin)
  (put 'or 'qeval disjoin)
  (put 'not 'qeval negate)
  (put 'lisp-value 'qeval lisp-value)
  (put 'unique 'qeval uniquely-asserted)
  (put 'always-true 'qeval always-true))

(define (qeval query stream-frame)
  "Evaluate query QUERY in stream of frames STREAM-FRAME."
  (let ((qproc (get (type query) 'qeval)))
    (if qproc
        (qproc (contents query) stream-frame)
        (simple-query query stream-frame))))

;;; REPL

;; Instantiate expression EXP relative to frame FRAME.  Use UNBOUND-VAR-HANDLER
;; to handle un-bound bindings.
(define (instantiate exp frame unbound-var-handler)
  (define (copy exp)
    (cond
     ((var? exp)
      (let ((binding (binding-in-frame exp frame)))
        (if binding
            (copy (binding-value binding))
            (unbound-var-handler exp frame))))
     ((pair? exp)
      (cons (copy (car exp)) (copy (cdr exp))))
     (else exp)))

  (copy exp))

(define (prompt-for-input string)
  (newline) (display string) (newline))

(define (announce-output string)
  (newline) (display string) (newline))

(define (query-driver-loop)
  "Run a query RELP."
  (prompt-for-input input-prompt)
  (let ((q (query-syntax-process (read))))
    (cond
     ((assertion? q)
      (qeval q (stream-singleton '()))
      (display "\nAssertion added to data base.\n")
      (query-driver-loop))
     (else
      (announce-output output-prompt)
      (stream-display
       (stream-map
	(lambda (frame)
	  (instantiate q frame (lambda (v f) (contract-question-mark v))))
        (qeval q (stream-singleton '()))))
      (query-driver-loop)))))

(define (initialize-database data)
  "Initialize database with rule and assertions data DATA."
  (define (deal-out r-and-a rules assertions)
    (cond
     ((null? r-and-a)
      (set! *assertions* (list->stream assertions))
      (set! *rules* (list->stream rules))
      'done)
     (else
      (let ((s (query-syntax-process (car r-and-a))))
        (cond
	 ((rule? s)
          (store-rule-in-index s)
          (deal-out (cdr r-and-a) (cons s rules) assertions))
         (else
          (store-assertion-in-index s)
          (deal-out (cdr r-and-a) rules (cons s assertions))))))))

  (deal-out data '() '()))

(install-qeval-package)

;;; query.scm ends here
