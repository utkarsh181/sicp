;; All people supervised by Ben Bitdiddle
(supervisor (Bitdiddle Ben) ?x)

;; All people working in accounting division
(job ?name (accounting . ?jobs))

;; All people working in Slumerville
(adddress ?name (Slumerville . ?address))
