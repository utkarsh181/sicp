(use-modules (sicp ch-4 ambeval))

;; NOTE 2021-09-19: Don't evaluate! This is a subset of Scheme implemented in a
;; non-deterministic (or ambiguous) evaluator in "ambeval.scm".

;; Return a Pythagorean triple with lowest number LOW.
(define (a-pythagorean-triple low)
  (let ((k (an-integer-starting-from low)))
    (let ((j (an-integer-between low (1- k))))
      (let ((i (an-integer-between low (1- j))))
	(require (= (+ (* i i) (* j j)) (* k k)))
	(list i j k)))))
