;; ambeval.scm --- Non-Deterministic Meta-Circular Evaluator (in/for Scheme).

;;; Commentary:

;; This generates a non-deterministic (or ambiguous) evaluator by introducing a
;; special form `amb' (syntax defined below) which combined with `require'
;; special form can be used to perform non-deterministic computing.  For
;; example, to ambiguously return an element of a list ITEMS, we can use:
;;
;; (define (an-element-of items)
;;   (require (not (null? items)))
;;   (amb (car items) (an-element-of (cdr items))))

;;; Code:

(define-module (sicp ch-4 ambeval)
  #:use-module (sicp ch-4 table)
  #:use-module (sicp ch-4 syntax)
  #:export (the-global-environment
	    execute-application
	    ambeval-eval
	    driver-loop))

(define input-prompt ";;; Amb-Eval input:")

(define output-prompt ";;; Amb-Eval value:")

;;; Constructors

(define (make-not expr)
  (list 'not expr))

(define (make-permanent-set! var body)
  (list 'permanent-set! var body))

;; Make a procedure expressin with formal parameter PARAMETER, body BODY in
;; environment ENV.
(define (make-procedure parameters body env)
  (list 'procedure parameters body env))

;;; Selectors

;; Syntax for Amb: (amb <e1> <e2> ... <e_n>)

;; Return choices of an AMB expression EXP.
(define (amb-choices exp) (cdr exp))

(define (require-predicate exp) (cadr exp))

;;; Predicates

;; Is expression EXP tagged TAG?
(define (tagged-list? exp tag)
  (if (pair? exp)
      (eq? (car exp) tag)
      #f))

(define (amb? exp) (tagged-list? exp 'amb))

;;; Transformers

;; Append VAR-VALUE pair into let expression EXP.
(define (let-append var value exp)
  ;; NOTE 2021-09-23: Take a look at `permanet-set!'.  This highlights some of
  ;; inner profound difference between ambiguous and normal evaluator.
  (if (null? exp)
      (make-let (list (list var (make-quoted '*unassigned*)))
		(make-permanent-set! var value))
      (append
       (list 'let
	     (cons (list var (make-quoted '*unassigned*)) (cadr exp)))
       (list (make-permanent-set! var value))
       (cddr exp))))

(define (letrec->let exp)
  (define (iter vars values exp)
    (if (or (null? vars) (null? values))
	exp
	(iter (cdr vars)
	      (cdr values)
	      (let-append (car vars) (car values) exp))))
  (let ((varlist (let-varlist exp)))
    (append (iter (car varlist) (cadr varlist) '())
	    (cdr exp))))

;;; Apply

;; Execute procedure PROC with arguments ARGS.
(define (execute-application proc args succeed fail)
  (cond
   ((primitive-procedure? proc)
    (succeed (apply-primitive-procedure proc args) fail))
   ((compound-procedure? proc)
    ((procedure-body proc)
     (extend-environment
      (procedure-parameters proc)
      args
      (procedure-environment proc))
     succeed
     fail))
   (else
    (error "Unknown procedure type -- EXECUTE-APPLICATION" proc))))

;;; Eval

;; Analyze a self-evaluating expression EXP.
(define (analyze-self-eval exp)
  (lambda (env succeed fail)
    (succeed exp fail)))

;; Analyze a quoted (i.e. (quote <exp>)) expression EXP.
(define (analyze-quoted exp)
  (let ((qval (text-of-quotation exp)))
    (lambda (env succeed fail)
      (succeed qval fail))))

;; Analyze a variable expression EXP.
(define (analyze-variable exp)
  (lambda (env succeed fail)
    (succeed (lookup-variable-value exp env)
             fail)))

;; Analyze a definition expression EXP.
(define (analyze-definition exp)
  (let ((var (definition-variable exp))
        (vproc (analyze (definition-value exp))))
    (lambda (env succeed fail)
      (vproc env
             (lambda (val fail2)
               (define-variable! var val env)
               (succeed 'ok fail2))
             fail))))

;; Analyze a assignment expression EXP.
(define (analyze-assignment exp)
  (let ((var (assignment-variable exp))
        (vproc (analyze (assignment-value exp))))
    (lambda (env succeed fail)
      (vproc
       env
       ;; If assignment fails, then put back the `old-value'.
       (lambda (val fail2)
         (let ((old-value (lookup-variable-value var env)))
           (set-variable-value! var val env)
           (succeed 'ok
		    (lambda ()
		      (set-variable-value! var old-value env)
		      (fail2)))))
       fail))))

;; Analyze an permanent assignment expression EXP.
(define (analyze-permanent-assignment exp)
  (let ((var (assignment-variable exp))
        (vproc (analyze (assignment-value exp))))
    (lambda (env succeed fail)
      (vproc
       env
       ;; Unlike `analyze-assignment', don't keep note of `old-value'.
       (lambda (val fail2)
         (set-variable-value! var val env)
         (succeed 'ok fail2))
       fail))))

;; Analyze an if expression EXP.
(define (analyze-if exp)
  (let ((pproc (analyze (if-predicate exp)))
        (cproc (analyze (if-consequent exp)))
        (aproc (analyze (if-alternative exp))))
    (lambda (env succeed fail)
      (pproc env
             ;; Success continuation for evaluating the predicate
             ;; to obtain pred-value
             (lambda (pred-value fail2)
               (if (true? pred-value)
                   (cproc env succeed fail2)
                   (aproc env succeed fail2)))
             ;; Failure continuation for evaluating the predicate
             fail))))

;; Analyze an sequence of expressions EXPS.
(define (analyze-sequence exps)
  (define (sequentially a b)
    (lambda (env succeed fail)
      (a env
         ;; Success continuation for calling a
         (lambda (a-value fail2)
           (b env succeed fail2))
         ;; Failure continuation for calling a
         fail)))

  (define (loop first-proc rest-procs)
    (if (null? rest-procs)
        first-proc
        (loop (sequentially first-proc (car rest-procs))
              (cdr rest-procs))))

  (let ((procs (map analyze exps)))
    (if (null? procs)
        (error "Empty sequence -- ANALYZE-SEQUENCE"))
    (loop (car procs) (cdr procs))))

;; Analyze lambda expression EXP.
(define (analyze-lambda exp)
  (let ((vars (lambda-parameters exp))
        (bproc (analyze-sequence (scan-out-defines (lambda-body exp)))))
    (lambda (env succeed fail)
      (succeed (make-procedure vars bproc env)
               fail))))

;; Analyze an begin expression EXP.
(define (analyze-begin exp)
  (let ((bproc (analyze-sequence (begin-actions exp))))
    (lambda (env succeed fail)
      (succeed (bproc env succeed fail) fail))))

;; Get list of args for argument procedures APROCS in enviornment ENV with
;; SUCCEED and FAIL as success and failure continuation.
(define (get-args aprocs env succeed fail)
  (if (null? aprocs)
      (succeed '() fail)
      ((car aprocs)
       env
       ;; success continuation for this aproc
       (lambda (arg fail2)
         (get-args (cdr aprocs)
                   env
                   ;; success continuation for recursive
                   ;; call to get-args
                   (lambda (args fail3)
                     (succeed (cons arg args) fail3))
                   fail2))
       fail)))

;; Analyze an application expression EXP.
(define (analyze-application exp)
  (let ((fproc (analyze (operator exp)))
        (aprocs (map analyze (operands exp))))
    (lambda (env succeed fail)
      (fproc env
	     (lambda (proc fail2)
               (get-args aprocs
			 env
			 (lambda (args fail3)
			   (execute-application
			    proc args succeed fail3))
			 fail2))
	     fail))))

;; Analyze an `cond' expression EXP.
(define (analyze-cond exp)
  (lambda (env succeed fail)
    ((analyze-if (cond->if exp)) env succeed fail)))

;; Analyze an `or' expression EXP.
(define (analyze-or exp)
  (lambda (env succeed fail)
    (define (iter aprocs previous-result)
      (if (null? aprocs)
	  (succeed previous-result fail)
	  (let ((result ((car aprocs) env succeed fail)))
	    (if result
		(succeed result fail)
		(iter (cdr aprocs) result)))))
    ;; NOTE 2021-09-14: We can optimize it even further, as we don't have to
    ;; analyze expression after first #t.
    (iter (map analyze (operands exp))
	  #f)))

;; Analyze an `and' expression EXP.
(define (analyze-and exp)
  (lambda (env succeed fail)
    (define (iter aprocs previous-result)
      (if (null? aprocs)
	  (succeed previous-result fail)
	  (let ((result ((car aprocs) env succeed fail)))
	    (if (not result)
		(succeed #f fail)
		(iter (cdr aprocs) result)))))
    ;; NOTE 2021-09-14: We can optimize it even further, as we don't have to
    ;; analyze expression after first #f.
    (iter (map analyze (operands exp))
	  #t)))

;; Analyze an `let' expression EXP.
(define (analyze-let exp)
  (analyze (let->combination (operands exp))))

;; Analyze an `let*' expression EXP.
(define (analyze-let* exp)
  (analyze (let*->nested-let (operands exp))))

;; Analyze an `letrec' expression EXP.
(define (analyze-letrec exp)
  (analyze (letrec->let (operands exp))))

;; Analyze an `while' expression EXP.
(define (analyze-while exp)
  (let ((aproc (analyze (while->combination (operands exp)))))
    (lambda (env succeed fail)
      (succeed (aproc env succeed fail) fail))))

;; Remove the binding of symbol-expression EXP from exvironment ENV.
(define (analyze-unbind! exp)
  (lambda (env succeed fail)
    (succeed (unbind-variable! (unbind-variable-name exp) env)
	     fail)))

;; Analyze an Amb expression EXP.
(define (analyze-amb exp)
  (let ((cprocs (map analyze (amb-choices exp))))
    (lambda (env succeed fail)
      ;; Analyze next choice in list of choices CHOICES.
      (define (try-next choices)
        (if (null? choices)
            (fail)
            ((car choices)
	     env
             succeed
             (lambda ()
               (try-next (cdr choices))))))

      (try-next cprocs))))

;; Analyze an require expression EXP.
(define (analyze-require exp)
  (let ((pproc (analyze (require-predicate exp))))
    (lambda (env succeed fail)
      (pproc env
	     (lambda (pred-value fail2)
	       (if (not pred-value)
		   (fail2))
		(succeed pred-value fail2))
	     fail))))

;; Syntax for if-fail: (if-fail <exp> <failure_continuation>)

;; Analyze an if-fail expression EXP.
(define (analyze-if-fail exp)
  (let ((pproc (analyze (if-predicate exp)))
	(cproc (analyze (if-consequent exp))))
    (lambda (env succed fail)
      (pproc env
	     (lambda (pred-value fail2)
	       (succed pred-value fail2))
	     (lambda ()
	       (cproc env succed fail))))))

;; Install of procedures which are used in analyzing expressions.
(define (install-analyze-package)
  (put 'analyze 'quote analyze-quoted)
  (put 'analyze 'set! analyze-assignment)
  (put 'analyze 'permanent-set! analyze-permanent-assignment)
  (put 'analyze 'define analyze-definition)
  (put 'analyze 'lambda analyze-lambda)
  (put 'analyze 'if analyze-if)
  (put 'analyze 'begin analyze-begin)
  (put 'analyze 'cond analyze-cond)
  (put 'analyze 'or analyze-or)
  (put 'analyze 'and analyze-and)
  (put 'analyze 'let analyze-let)
  (put 'analyze 'let* analyze-let*)
  (put 'analyze 'letrec analyze-letrec)
  (put 'analyze 'while analyze-while)
  (put 'analyze 'unbind! analyze-unbind!)
  (put 'analyze 'amb analyze-amb)
  (put 'analyze 'require analyze-require)
  (put 'analyze 'if-fail analyze-if-fail))

;; Return the syntactic analysis of expression EXP.
(define (analyze exp)
  (cond
   ((self-evaluating? exp)
    (analyze-self-eval exp))
   ((variable? exp) (analyze-variable exp))
   ((get 'analyze (operator exp))
    => (lambda (proc) (proc exp)))
   ((application? exp) (analyze-application exp))
   (else
    (error "Unknown expression type -- ANALYZE" exp))))

;; Evaluate expression EXP in enviornment ENV.
(define (ambeval-eval exp env succeed fail)
  ((analyze exp) env succeed fail))

;;; REPL

(define (prompt-for-input string)
  (newline) (newline) (display string) (newline))

(define (announce-output string)
  (newline) (display string) (newline))

(define (user-print object)
  (if (compound-procedure? object)
      (display (list 'compound-procedure
                     (procedure-parameters object)
                     (procedure-body object)
                     '<procedure-env>))
      (display object)))

;; Run a Read-Eval-Print-Loop.
(define (driver-loop)
  (define (internal-loop try-again)
    (prompt-for-input input-prompt)
    (let ((input (read)))
      (if (eq? input 'try-again)
          (try-again)
          (begin
            (newline)
            (display ";;; Starting a new problem ")
            (ambeval-eval
	     input
             the-global-environment
             ;; ambeval success
             (lambda (val next-alternative)
               (announce-output output-prompt)
               (user-print val)
               (internal-loop next-alternative))
             ;; ambeval failure
             (lambda ()
               (announce-output
                ";;; There are no more values of")
               (user-print input)
               (driver-loop)))))))
  (internal-loop
   (lambda ()
     (newline)
     (display ";;; There is no current problem")
     (driver-loop))))

(define (setup-environment)
  (let ((initial-env
	 (extend-environment (primitive-procedure-names)
			     (primitive-procedure-objects)
			     the-empty-environment)))
    (define-variable! #t #t initial-env)
    (define-variable! #f #f initial-env)
    (install-analyze-package)
    initial-env))

(define the-global-environment (setup-environment))

;;; ambeval.scm ends here
