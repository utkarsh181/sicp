;; List all meeting on Friday.
(metting ?department (Friday ?time))

;; Metting time for person ?PERSON on ?DAY-AND-TIME.
(rule (metting-time ?person ?day-and-time)
      (and (job ?person (?department . ?rest-1))
	   (or (metting ?department ?day-and-time)
	       (metting whole-company ?day-and-time))))

;; List of metting of Alyssa P. Hacker on Wednesday.
(metting-time (Hacker Alyssa P) (Wednesday ?time))
