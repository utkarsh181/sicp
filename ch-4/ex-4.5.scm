;;; Prototype for alternative syntax for `cond'.  Also see "beval.scm".

(define (cond-extended-syntax? clause)
  (and (pair? clause)
       (< (length clause) 2)
       (eq? (cadr clause) '=>)))

(define (cond-extended-predicate clause) (car clause))

(define (cond-extended-action clause) (caddr clause))

;; Transform sequence of expression SEQ into single expression using `begin' if
;; necessary.
(define (sequence->exp seq)
  (cond ((null? seq) seq)
        ((last-exp? seq) (first-exp seq))
        (else (make-begin seq))))

(define (expand-clauses clauses)
  (if (null? clauses)
      'false                          ; no else clause
      (let ((first (car clauses))
	    (rest (cdr clauses)))
	(cond
	 ((cond-else-clause? first)
	  (if (null? rest)
	      (sequence->exp (cond-actions first))
              (error "ELSE clause isn't last -- COND->IF"
                     clauses)))
	 ((cond-extended-syntax? first)
	  (make-if (cond-extended-predicate first)
		   (list (cond-extended-predicate first)
			 (cond-extended-action first))
		   (expand-clauses rest)))
	 (else
	  (make-if (cond-predicate first)
		   (sequence->exp (cond-actions first))
		   (expand-clauses rest)))))))

;; Tranform a cond expression EXP into if expression.
(define (cond->if exp)
  (expand-clauses (cond-clauses exp)))
