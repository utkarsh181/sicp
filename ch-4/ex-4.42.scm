;;; Solution to Liar's puzzle with non-deterministic computing.

(define (xor a b)
  (and (or a b) (not (and a b))))

(define (require p) (if (not p) (amb)))

;; REVIEW 2021-09-21: Verify it's result.
(define (liars-puzzle)
  (let ((betty (amb 1 2 3 4 5))
	(kitty (amb 1 2 3 4 5)))
    (require (xor (= kitty 2) (= betty 3)))
    (let ((mary (amb 1 2 3 4 5)))
      (require (xor (= kitty 2) (= mary 4)))
      (require (xor (= mary 4) (= betty 1)))
      (let ((ethel (amb 1 2 3 4 5))
	    (joan (amb 1 2 3 4 5)))
	(require (xor (= ethel 1) (= joan 2)))
	(require (xor (= joan 3) (= ethel 5)))
	(require (distinct? (list betty ethel joan kitty mary)))
	(list (list 'betty betty)
	      (list 'ethel ethel)
	      (list 'joan joan)
	      (list 'kitty kitty)
	      (list 'mary mary))))))

;; => ((betty 5) (ethel 1) (joan 3) (kitty 2) (mary 4))
