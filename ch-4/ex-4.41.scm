;;; Multiple-dwelling problem.  Also see "ex-4.38-39-40.scm".

(define possible-floors '((cooper (2 3 4 5))
			  (miller (1 2 3 4 5))
			  (fletcher (2 3 4))
			  (smith (1 2 3 4 5))
			  (baker (1 2 3 4 5))))

;; Are all elements of list ITEMS are distinct?
(define (distinct? items)
  (cond ((null? items) #t)
	((null? (cdr items)) #t)
	((member (car items) (cdr items)) #f)
	(else (distinct? (cdr items)))))

;; Is list LST a valid representation of multiple dwelling problem.
(define (valid? lst)
  (and
   (distinct? (map cdr lst))
   (> (assoc-ref lst 'miller) (assoc-ref lst 'cooper))
   (not (= 1 (abs (- (assoc-ref lst 'smith) (assoc-ref lst 'fletcher)))))
   (not (= 1 (abs (- (assoc-ref lst 'fletcher) (assoc-ref lst 'cooper)))))))

;; Recursively check floors FLOORS and accumulate result in association list
;; ANS.
(define (rec-floors floors ans)
    (if (null? floors)
	(when (valid? ans)
	  (throw 'final-ans ans))
	(let* ((top (car floors))
	       (name (car top))
	       (pfloors (cadr top)))
	  (map
	   (lambda (x)
	     (rec-floors (cdr floors) (cons (cons name x) ans)))
	   pfloors))))

(define (multiple-dwell)
  (catch 'final-ans
    (lambda () (rec-floors possible-floors '()))
    (lambda (key . args) (car args))))
