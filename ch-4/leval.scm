;;; leval.scm --- Lazy Meta-Circular Evaluator (in/for Scheme)

;;; Commentary:

;; To invoke lazy evaluation add `lazy' or `lazy-memo' keyword in formal
;; parameters in procedure definition which means "lazy evaluation" and "lazy
;; evaluation with memoization" respectively.  For example:
;;
;; (define (foo a (b lazy) c (d lazy-memo))
;;   ...)

;;; Code:

(define-module (sicp ch-4 leval)
  #:use-module (sicp ch-4 table)
  #:use-module (sicp ch-4 syntax)
  #:export (the-global-environment
	    actual-value
	    leval-apply
	    driver-loop))

;; REVIEW 2021-09-16: Just like "beval.scm", can we separate syntactic analysis
;; here? Or am I confusing between two really orthogonal concepts? In an
;; essence, analysis work on expression and laziness works on (expression +
;; environment), so it is possible that producing "thunks" an analysis time is a
;; really bad idea.

(define input-prompt ";;; L-Eval input:")

(define output-prompt ";;; L-Eval value:")

;;; Constructor

;; Make a delay of expression EXP in enviornment ENV.
(define (make-delay exp env)
  (list 'thunk exp env))

;; Make a delay of expression EXP in enviornment ENV which is capable if
;; memoization.
(define (make-memo-delay exp env)
  (list 'mthunk exp env))

;;; Selectors

(define (thunk-exp thunk) (cadr thunk))

(define (thunk-env thunk) (caddr thunk))

(define (thunk-value evaluated-thunk) (cadr evaluated-thunk))

;; Return value of thunk object OBJ by forcing it.
(define (force-it obj)
  (cond
   ((thunk? obj)
    (actual-value (thunk-exp obj) (thunk-env obj)))
   ((mthunk? obj)
    (let ((result (actual-value (thunk-exp obj) (thunk-env obj))))
      (set-car! obj 'evaluated-thunk)
      (set-car! (cdr obj) result)  ;replace exp with its value
      (set-cdr! (cdr obj) '())     ;forget unneeded env
      result))
   ((evaluated-thunk? obj)
    (thunk-value obj))
   (else obj)))

(define (actual-value exp env)
  (force-it (leval-eval exp env)))

;;; Predicates

(define (tagged-list? exp tag)
  (if (pair? exp)
      (eq? (car exp) tag)
      #f))

;; Is object OBJ a thunk?
(define (thunk? obj)
  (tagged-list? obj 'thunk))

(define (mthunk? obj)
  (tagged-list? obj 'mthunk))

;; Is thunk of object OBJ already evaluated?
(define (evaluated-thunk? obj)
  (tagged-list? obj 'evaluated-thunk))

;;; Apply

(define (list-of-arg-values exps env)
  (if (no-operands? exps)
      '()
      (cons (actual-value (first-operand exps) env)
	    (list-of-arg-values (rest-operands exps) env))))

;; Extend environment of procedure PROCEDURE by scanning argument expressions
;; EXPS in environment ENV.
(define (procedure-extend-environment procedure exps env)
  (define (iter parameters exps pres eres)
    (if (and (null? parameters) (null? exps))
	(extend-environment
	 (reverse pres) (reverse eres) (procedure-environment procedure))
	(let ((p (car parameters))
	      (e (car exps)))
	  (cond
	   ((symbol? p)
	    (iter (cdr parameters)
		  (cdr exps)
		  (cons p pres)
		  (cons (actual-value e env) eres)))
	   ((and (pair? p) (= (length p) 2))
	    (cond
	     ((eq? (cadr p) 'lazy)
	      (iter (cdr parameters)
		    (cdr exps)
		    (cons (car p) pres)
		    (cons (make-delay e env) eres)))
	     ((eq? (cadr p) 'lazy-memo)
	      (iter (cdr parameters)
		    (cdr exps)
		    (cons (car p) pres)
		    (cons (make-memo-delay e env) eres)))))
	   (else (error "Unknown parameter type -- PROCEDURE-EXTEND-ENVIRONMENT"))))))

  (iter (procedure-parameters procedure) exps '() '()))

(define (leval-apply procedure arguments env)
  (cond
   ((primitive-procedure? procedure)
    (apply-primitive-procedure
     procedure
     (list-of-arg-values arguments env)))
   ((compound-procedure? procedure)
    (eval-sequence
     (procedure-body procedure)
     (procedure-extend-environment procedure arguments env)))
   (else (error "Unknown procedure type -- LEVAL-APPLY" procedure))))

;;; Eval

(define (eval-if exp env)
  (if (true? (actual-value (if-predicate exp) env))
      (leval-eval (if-consequent exp) env)
      (leval-eval (if-alternative exp) env)))

(define (eval-sequence exps env)
  (cond
   ((last-exp? exps)
    (leval-eval (first-exp exps) env))
   (else
    (leval-eval (first-exp exps) env)
    (eval-sequence (rest-exps exps) env))))

(define (eval-assignment exp env)
  (set-variable-value!
   (assignment-variable exp)
   (leval-eval (assignment-value exp) env)
   env))

(define (eval-definition exp env)
  (define-variable!
    (definition-variable exp)
    (leval-eval (definition-value exp) env)
    env))

;; Return value of operands in expression EXP in environment ENV.
(define (list-of-values exp env)
  (if (no-operands? exp)
      '()
      ;; NOTE 2021-09-09: Explicit use of `left' and 'right' denote order of
      ;; evaluation.
      (let ((left (leval-eval (first-operand exp) env))
	    (right (list-of-values (rest-operands exp) env)))
	(cons left right))))

(define (eval-quoted exp env)
  (text-of-quotation exp))

(define (eval-lambda exp env)
  (make-procedure (lambda-parameters exp)
                  (lambda-body exp)
                  env))

(define (eval-begin exp env)
  (eval-sequence (begin-actions exp) env))

(define (eval-cond exp env)
  (leval-eval (cond->if exp) env))

(define (eval-or exp env)
  (define (iter current-exp previous-result)
    (if (no-operands? current-exp)
	previous-result
	(let ((result (leval-eval (first-operand current-exp) env)))
	  (if result
	      result
	      (iter (rest-operands current-exp) result)))))
  (iter (operands exp) #f))

(define (eval-and exp env)
  (define (iter current-exp previous-result)
    (if (no-operands? current-exp)
	previous-result
	(let ((result (leval-eval (first-operand current-exp) env)))
	  (if (not result)
	      #f
	      (iter (rest-operands current-exp) result)))))
  (iter (operands exp) #t))

(define (eval-let exp env)
  (leval-eval (let->combination (operands exp)) env))

(define (eval-let* exp env)
  (leval-eval (let*->nested-let (operands exp)) env))

(define (eval-letrec exp env)
  (leval-eval (letrec->let (operands exp)) env))

;; Eval a 'while' expression EXP in environment ENV.
(define (eval-while exp env)
  (leval-eval (while->combination (operands exp)) env))

;; Remove the binding of symbol-expression EXP from exvironment ENV.
(define (eval-unbind! exp env)
  ;; TODO 2021-09-12: Can we complain about false input?
  (unbind-variable! (car (operands exp)) env))

(define (install-eval-package)
  (put 'eval 'quote eval-quoted)
  (put 'eval 'set! eval-assignment)
  (put 'eval 'define eval-definition)
  (put 'eval 'lambda eval-lambda)
  (put 'eval 'if eval-if)
  (put 'eval 'begin eval-begin)
  (put 'eval 'cond eval-cond)
  (put 'eval 'or eval-or)
  (put 'eval 'and eval-and)
  (put 'eval 'let eval-let)
  (put 'eval 'let* eval-let*)
  (put 'eval 'letrec eval-letrec)
  (put 'eval 'while eval-while)
  (put 'eval 'unbind! eval-unbind!))

;; Eval expression EXP in enviornment ENV.
(define (leval-eval exp env)
  (cond
   ((self-evaluating? exp) exp)
   ((variable? exp) (lookup-variable-value exp env))
   ((get 'eval (operator exp))
    => (lambda (proc) (proc exp env)))
   ((application? exp)
    (leval-apply
     (actual-value (operator exp) env)
     (operands exp)
     env))
   (else
    (error "Unknown expression type -- LEVAL-EVAL" exp))))

;;; REPL

(define (prompt-for-input string)
  (newline) (newline) (display string) (newline))

(define (announce-output string)
  (newline) (display string) (newline))

(define (user-print object)
  (if (compound-procedure? object)
      (display (list 'compound-procedure
                     (procedure-parameters object)
                     (procedure-body object)
                     '<procedure-env>))
      (display object)))

(define (driver-loop)
  (prompt-for-input input-prompt)
  (let ((input (read)))
    (let ((output (actual-value input the-global-environment)))
      (announce-output output-prompt)
      (user-print output)))
  (driver-loop))

(define (setup-environment)
  (let ((initial-env (extend-environment
		      (primitive-procedure-names)
                      (primitive-procedure-objects)
                      the-empty-environment)))
    (define-variable! #t #t initial-env)
    (define-variable! #f #f initial-env)
    (install-eval-package)
    initial-env))

(define the-global-environment (setup-environment))

;;; leval.scm ends here
