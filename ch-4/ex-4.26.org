#+title: SICP Exercise 4.25
#+author: Utkarsh Singh

* Unless as procedure

#+begin_src scheme
(define (unless condition usual-value exceptional-value)
  (if condition exceptional-value usual-value))
#+end_src

* Unless as derived expression

#+begin_src scheme
(define (eval-unless exp env)
  (eval-if (make-if (unless-predicate exp)
		    (unless-usual exp)
		    (unless-exceptional exp))
	   env))
#+end_src

* Conclusion

I would like to take Ben's side here, and would implement =unless= as
derived expression, as I cannot think of a single situation where its
procedure implementation could be useful.  In GNU Guile, it is
implemented as a macro:

#+begin_src scheme
(define-syntax-rule (unless condition stmt stmt* ...)
  (if (not test) (begin stmt stmt* ...)))
#+end_src
