;;; stream.scm --- Stream library

;;; Code

(define-module (sicp ch-4 stream)
  #:use-module (srfi srfi-41)
  #:export (interleave-delayed
	    stream-singleton
	    stream-flatmap
	    stream-simple-flatmap
	    stream-append-delayed
	    stream-display
	    stream-equal-length?))

(define (stream-singleton x)
  (stream-cons x stream-null))

(define (interleave-delayed s1 delayed-s2)
  (if (stream-null? s1)
      (force delayed-s2)
      (stream-cons
       (stream-car s1)
       (interleave-delayed (force delayed-s2)
                           (delay (stream-cdr s1))))))

(define (stream-flatten stream)
  (if (stream-null? stream)
      stream-null
      (interleave-delayed
       (stream-car stream)
       (delay (stream-flatten (stream-cdr stream))))))

(define (stream-flatmap proc s)
  (stream-flatten (stream-map proc s)))


(define (stream-simple-flatten stream)
  (stream-map stream-car
	      (stream-filter (lambda (s) (not (stream-null? s)))
			     stream)))

(define (stream-simple-flatmap proc s)
  (stream-simple-flatten (stream-map proc s)))

(define (stream-append-delayed s1 delayed-s2)
  (if (stream-null? s1)
      (force delayed-s2)
      (stream-cons
       (stream-car s1)
       (stream-append-delayed (stream-cdr s1) delayed-s2))))

(define (stream-display s)
  (stream-for-each (lambda (x) (display x) (newline))
		   s))

(define (stream-equal-length? strm len)
    (if (not (stream? strm))
        (error 'stream-length "non-stream argument")
        (let loop ((count 0) (strm strm))
	  (cond
	   ((and (= count len) (stream-null? strm)) #t)
	   ((or (> count len) (stream-null? strm)) #f)
	   (else
            (loop (+ count 1) (stream-cdr strm)))))))

;;; stream.scm ends here
