;;; Append

(assert! (rule (append-to-form () ?y ?y)))

(assert! (rule (append-to-form (?u . ?v) ?y (?u . ?z))
			   (append-to-form ?v ?y ?z)))

;;; Reverse

(assert! (rule (reverse () ())))

;; Is ?ORIG (?orig-car + ?orig-cdr) reverse of ?REV
(assert! (rule (reverse (?orig-car . ?orig-cdr) ?rev)
	       (and (append-to-form ?rev-cdr (?orig-car) ?rev)
		    (reverse ?orig-cdr ?rev-cdr))))
