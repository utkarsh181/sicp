;;; Prototype for `letrec'.  Also see "beval.scm".

(define (letrec->let exp)
  (define (iter vars values exp)
    (if (or (null? vars) (null? values))
	exp
	(iter (cdr vars)
	      (cdr values)
	      (let-append (car vars) (car values) exp))))
  (let ((varlist (let-varlist exp)))
    (append (iter (car varlist) (cadr varlist) '())
	    (cdr exp))))

(define (eval-letrec exp env)
  (beval-eval (letrec->let (operands exp)) env))
