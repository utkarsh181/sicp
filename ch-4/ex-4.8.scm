;;; Prototype for named `let' syntax.  Also see "beval.scm"

(define (let-named-syntax? exp)
  (not (pair? (car exp))))

;; Tranform let expression EXP into a combination.
(define (let->combination exp)
  ;; TODO 2021-09-11: Cleanup!
  (if (let-named-syntax? exp)
      (let* ((name (car exp))
	     (var-list (let-varlist (cdr exp)))
	     (var (car var-list))
	     (expr (cadr var-list)))
	(sequence->exp
	 (list (make-define (cons name var) (let-body (cdr exp)))
	       (cons name expr))))
      (let* ((var-list (let-varlist exp))
	     (var (car var-list))
	     (expr (cadr var-list)))
	(cons (make-lambda var (let-body exp)) expr))))
