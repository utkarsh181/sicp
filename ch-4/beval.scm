;;; beval.scm --- Basic Meta-Circular Evaluator (in/for Scheme).

;;; Commentary

;;; Code:

(define-module (sicp ch-4 beval)
  #:use-module (sicp ch-4 table)
  #:use-module (sicp ch-4 syntax)
  #:export (the-global-environment
	    execute-application
	    beval-eval
	    driver-loop))

(define input-prompt ";;; M-eval input:")

(define output-prompt ";;; M-eval value:")

;;; Constructor

;; Make a procedure expressin with formal parameter PARAMETER, body BODY in
;; environment ENV.
(define (make-procedure parameters body env)
  (list 'procedure parameters body env))

;;; Apply

;; Execute procedure PROC with arguments ARGS.
(define (execute-application proc args)
  (cond
   ((primitive-procedure? proc)
    (apply-primitive-procedure proc args))
   ((compound-procedure? proc)
    ((procedure-body proc)
     (extend-environment
      (procedure-parameters proc)
      args
      (procedure-environment proc))))
   (else
    (error "Unknown procedure type -- EXECUTE-APPLICATION" proc))))

;;; Eval

(define (analyze-self-evaluating exp)
  (lambda (env) exp))

(define (analyze-quoted exp)
  (let ((qval (text-of-quotation exp)))
    (lambda (env) qval)))

(define (analyze-variable exp)
  (lambda (env) (lookup-variable-value exp env)))

(define (analyze-definition exp)
  (let ((var (definition-variable exp))
        (vproc (analyze (definition-value exp))))
    (lambda (env)
      (define-variable! var (vproc env) env)
      'ok)))

(define (analyze-assignment exp)
  (let ((var (assignment-variable exp))
	(vproc (analyze (assignment-value exp))))
    (lambda (env)
      (set-variable-value! var (vproc env) env)
      'ok)))

(define (analyze-if exp)
  (let ((pproc (analyze (if-predicate exp)))
        (cproc (analyze (if-consequent exp)))
        (aproc (analyze (if-alternative exp))))
    (lambda (env)
      (if (true? (pproc env))
          (cproc env)
          (aproc env)))))

(define (analyze-sequence exps)
  (define (sequentially proc1 proc2)
    (lambda (env) (proc1 env) (proc2 env)))

  (define (loop first-proc rest-procs)
    (if (null? rest-procs)
        first-proc
        (loop (sequentially first-proc (car rest-procs))
              (cdr rest-procs))))

  (let ((procs (map analyze exps)))
    (if (null? procs)
        (error "Empty sequence -- ANALYZE"))
    (loop (car procs) (cdr procs))))

(define (analyze-lambda exp)
  (let ((vars (lambda-parameters exp))
        (bproc (analyze-sequence (scan-out-defines (lambda-body exp)))))
    (lambda (env) (make-procedure vars bproc env))))

(define (analyze-begin exp)
  (let ((bproc (analyze-sequence (begin-actions exp))))
    (lambda (env)
      (bproc env))))

(define (analyze-application exp)
  (let ((fproc (analyze (operator exp)))
        (aprocs (map analyze (operands exp))))
    (lambda (env)
      (execute-application
       (fproc env)
       (map (lambda (aproc) (aproc env))
            aprocs)))))

(define (analyze-cond exp)
  (lambda (env)
    ((analyze-if (cond->if exp)) env)))

(define (analyze-or exp)
  (lambda (env)
    (define (iter aprocs previous-result)
      (if (null? aprocs)
	  previous-result
	  (let ((result ((car aprocs) env)))
	    (if result
		result
		(iter (cdr aprocs) result)))))
    ;; NOTE 2021-09-14: We can optimize it even further, as we don't have to
    ;; analyze expression after first #t.
    (iter (map analyze (operands exp))
	  #f)))

(define (analyze-and exp)
  (lambda (env)
    (define (iter aprocs previous-result)
      (if (null? aprocs)
	  previous-result
	  (let ((result ((car aprocs) env)))
	    (if (not result)
		#f
		(iter (cdr aprocs) result)))))
    ;; NOTE 2021-09-14: We can optimize it even further, as we don't have to
    ;; analyze expression after first #f.
    (iter (map analyze (operands exp))
	  #t)))

(define (analyze-let exp)
  (analyze (let->combination (operands exp))))

(define (analyze-let* exp)
  (analyze (let*->nested-let (operands exp))))

(define (analyze-letrec exp)
  (analyze (letrec->let (operands exp))))

(define (analyze-while exp)
  (lambda (env)
    ((analyze (while->combination (operands exp))) env)))

;; Remove the binding of symbol-expression EXP from exvironment ENV.
(define (analyze-unbind! exp)
  (lambda (env)
    (unbind-variable! (unbind-variable-name exp) env)))

;; Install procedures which are used in analyzing expressions.
(define (install-analyze-package)
  (put 'analyze 'quote analyze-quoted)
  (put 'analyze 'set! analyze-assignment)
  (put 'analyze 'define analyze-definition)
  (put 'analyze 'lambda analyze-lambda)
  (put 'analyze 'if analyze-if)
  (put 'analyze 'begin analyze-begin)
  (put 'analyze 'cond analyze-cond)
  (put 'analyze 'or analyze-or)
  (put 'analyze 'and analyze-and)
  (put 'analyze 'let analyze-let)
  (put 'analyze 'let* analyze-let*)
  (put 'analyze 'letrec analyze-letrec)
  (put 'analyze 'while analyze-while)
  (put 'analyze 'unbind! analyze-unbind!))

;; Return the syntactic analysis of expression EXP.
(define (analyze exp)
  (cond
   ((self-evaluating? exp)
    (analyze-self-evaluating exp))
   ((variable? exp) (analyze-variable exp))
   ((get 'analyze (operator exp))
    => (lambda (proc) (proc exp)))
   ((application? exp) (analyze-application exp))
   (else
    (error "Unknown expression type -- ANALYZE" exp))))

;; Evaluate expression EXP in enviornment ENV.
(define (beval-eval exp env)
  ((analyze exp) env))

;;; REPL

(define (prompt-for-input string)
  (newline) (newline) (display string) (newline))

(define (announce-output string)
  (newline) (display string) (newline))

(define (user-print object)
  (if (compound-procedure? object)
      (display (list 'compound-procedure
                     (procedure-parameters object)
                     (procedure-body object)
                     '<procedure-env>))
      (display object)))

;; Run a Read-Eval-Print-Loop.
(define (driver-loop)
  (prompt-for-input input-prompt)
  (let ((input (read)))
    (let ((output (beval-eval input the-global-environment)))
      (announce-output output-prompt)
      (user-print output)))
  (driver-loop))

(define (setup-environment)
  (let ((initial-env
	 (extend-environment (primitive-procedure-names)
			     (primitive-procedure-objects)
			     the-empty-environment)))
    (define-variable! #t #t initial-env)
    (define-variable! #f #f initial-env)
    (install-analyze-package)
    initial-env))

(define the-global-environment (setup-environment))

;;; beval.scm ends here
