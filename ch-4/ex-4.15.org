#+title: SICP Exercise 4.15
#+author: Utkarsh Singh

* Problem

#+begin_src scheme
(define (run-forever) (run-forever))

(define (try p)
  (if (halts? p p) (run-forever) 'halted))
#+end_src

* Solution

# NOTE 2021-09-12: A procedure `p' is said to halt on `a' if
# evaluation of expression `(p a)' return a value.

Yes, it is impossible to write a procedure =halt?=, that can determine
whether a procedure =p= halts on an object =a=, because it is
impossible to check if a procedure return a value or not.

To prove this we will follow the method of contradiction by analysing
the evaluation of =(try try)=.

# NOTE 2021-09-12: Proof by contradiction was inspired by Oavis at
# <https://www.inchmeal.io/sicp/ch-4/ex-4.15>

+ =Halts?= return true :: Then =run-forever= will be executed and thus
  this result it false.
+ =Halts?= return false :: Then symbol =halted= will be returned and
  again result in false.

Hence, it is impossible to implement =halt?=.
