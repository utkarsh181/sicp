;;; Prototype for `while'.  Also see "beval.scm".

(define (while-predicate exp) (car exp))

(define (while-body exp) (cdr exp))

(define (while->combination exp)
  (sequence->exp
   (list
    (make-define
     (list 'iter)
     (make-if (while-predicate exp)
	      (sequence->exp (list (while-body exp) '(iter)))
	      'true))
    '(iter))))
