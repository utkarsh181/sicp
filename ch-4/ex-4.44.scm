;;; Eight-queens puzzle solution in non-deterministic computing.

;;; By Eli Bendersky at:
;;; <https://eli.thegreenplace.net/2008/01/05/sicp-section-432>.  Earlier I was
;;; using unnecessary complex filtering which was bug-prone and slow.

;; Enumerate sequence of first N natural numbers.
(define (enumerate-seq n)
    (define (enum-seq-iter seq)
      (if (> seq n)
        '()
        (cons seq (enum-seq-iter (+ seq 1)))))

    (enum-seq-iter 1))

;; Ambiguously return elements of list LST.
(define (list-amb lst)
  (if (null? lst)
      (amb)
      (amb (car lst) (list-amb (cdr lst)))))

;; Return N'th element of list LST.
(define (nth n lst)
    (cond ((null? lst) '())
          ((= n 0) (car lst))
          (else (nth (- n 1) (cdr lst)))))

;; Does queen at (row1, col1) attacks queen at (row2, col2).
(define (attacks? row1 col1 row2 col2)
  (cond
   ((= row1 row2) #t)
   ((= col1 col2) #t)
   ;; Dignonal attack.
   ((= (abs (- col1 col2))
       (abs (- row1 row2))) #t)
   (else #f)))

;; Is queen at K'th position safe from all other queens?
(define (safe-kth? k pos)
  (let ((kth-col (nth k pos))
        (pos-len (length pos)))
    (define (safe-iter i)
      (cond ((= i pos-len) #t)
            ((= i k) (safe-iter (+ i 1)))
            (else
             (let ((ith-col (nth i pos)))
               (if (attacks? i ith-col k kth-col)
                   #f
                   (safe-iter (+ i 1)))))))

    (safe-iter 0)))

;; Return solution of queens puzzle solution for board-size N.
(define (queens n)
  (define (queens-iter pos i)
    (cond ((> i (- n 1)) pos)
          (else
           (let ((new-col (list-amb (enumerate-seq n))))
             (let ((new-pos (append pos (list new-col))))
               (require (safe-kth? i new-pos))
               (queens-iter new-pos (+ i 1)))))))

  (queens-iter '() 0))
