#+title: SICP Exercise 4.61
#+author: Utkarsh Singh

* Problem

#+begin_src lisp-data
(rule (?x next-to ?y in (?x ?y . ?u)))
(rule (?x next-to ?y in (?v . ?z))
      (?x next-to ?y in ?z))

(?x next-to ?y in (1 (2 3) 4))		;1
(?x next-to 1 in (2 1 3 1))		;2
#+end_src

* Result of 1

#+begin_src lisp-data
(1 next-to (2 3) in (1 (2 3) 4))
((2 3) next-to 4 in ((2 3) 4))
(4 next-to () in (4))
#+end_src

* Result of 2

#+begin_src lisp-data
(2 next-to 1 in (2 1 3 1))
(3 next-to 1 in (3 1))
(() next-to 1 in (1))
#+end_src
