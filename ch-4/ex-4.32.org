#+title: SICP Exercise 4.32
#+author: Utkarsh Singh

Examples similar to [[file:ex-4.27.org][Exercise 4.27]]:

#+begin_src scheme
(define (lcons (x lazy) (y lazy)) (lambda (m) (m x y)))
(define (lcar (z lazy)) (z (lambda (p q) p)))
(define (lcdr (z lazy)) (z (lambda (p q) q)))

(define count 0)
(define (id x) (set! count (+ count 1)) x)
(define foo (lcons (id 10) (id 10)))
;;; L-Eval input:
count
;;; L-Eval ouput
0
#+end_src

In our even more "lazier" list, compared to =stream=, both =car= and
=cdr= are not evaluated.  Hence, even after definition of lazy list
=foo=, =count= remains 0.
