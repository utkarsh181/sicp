;;; Prototype for `or' and `and'.  Also see "beval.scm".

(define (eval-or exp env)
  (define (iter current-exp previous-result)
    (if (no-operands? current-exp)
	previous-result
	(let ((result (beval-eval (first-operand current-exp) env)))
	  (if result
	      #t
	      (iter (rest-operands current-exp) result)))))
  (iter (operands exp) #f))

(define (eval-and exp env)
  (define (iter current-exp previous-result)
    (if (no-operands? current-exp)
	previous-result
	(let ((result (beval-eval (first-operand current-exp) env)))
	  (if (not result)
	      #f
	      (iter (rest-operands current-exp) result)))))
  (iter (operands exp) #t))
