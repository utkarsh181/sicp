;;; table.el --- Operation tables

;;; Commentary:

;;; Code:

(define-module (sicp ch-4 table)
  #:export (op-table put get))

;;; Table operation

;; Operation's hash table
(define op-table (make-hash-table))

;; Add generic operation OP on type TYPE through procedure PROC
(define (put op type proc)
  (hash-set! op-table (list op type) proc))

;; Get generic operation OP on type TYPE
(define (get op type)
  (hash-ref op-table (list op type) #f))

;;; table.el ends here
