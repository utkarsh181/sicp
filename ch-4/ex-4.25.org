#+title: SICP Exercise 4.25
#+author: Utkarsh Singh

* Problem

#+begin_src scheme
(define (unless condition usual-value exceptional-value)
  (if condition exceptional-value usual-value))

(define (factorial n)
  (unless (= n 1)
    (* n (factorial (- n 1)))
    1))
#+end_src

* Solution

In applicative-order evaluation, invocation to =factorial=  will lead
to infinite recursion, whereas in normal-oder evaluation =(factorial
5)= will procedure 120.
