;;; Prototype for data-directed `eval'.  Also see "beval.scm".

(define (eval-quoted exp env)
  (text-of-quotation exp))

(define (eval-lambda exp env)
  (make-procedure (lambda-parameters exp)
                  (lambda-body exp)
                  env))

(define (eval-begin exp env)
  (eval-sequence (begin-actions exp) env))

(define (eval-cond exp env)
  (beval-eval (cond->if exp) env))

(define (install-eval-package)
  (put 'eval 'quote eval-quoted)
  (put 'eval 'set! eval-assignment)
  (put 'eval 'define eval-definition)
  (put 'eval 'lambda eval-lambda)
  (put 'eval 'if eval-if)
  (put 'eval 'begin eval-begin)
  (put 'eval 'cond eval-cond))

;; Eval expression EXP in enviornment ENV.
(define (beval-eval exp env)
  (cond
   ((self-evaluating? exp) exp)
   ((variable? exp) (lookup-variable-value exp env))
   ((get 'eval (operator exp))
    ((get 'eval (operator exp)) exp env))
   ((application? exp)
    (beval-apply
     (beval-eval (operator exp) env)
     (list-of-values (operands exp) env)))
   (else
    (error "Unknown expression type -- BEVAL-EVAL" exp))))

(define (setup-environment)
  (let ((initial-env
         (extend-environment (primitive-procedure-names)
                             (primitive-procedure-objects)
                             the-empty-environment)))
    (define-variable! 'true #t initial-env)
    (define-variable! 'false #f initial-env)

    (install-eval-package)

    initial-env))
