;; List of people supervised by Ben Bitdiddle along with their address.
(and (supervisor (Bitdiddle Ben) ?x) (address ?x ?address))

;; All those people whoose salary is less than Ben Bitdiddle along with Ben's
;; salary.
(and (salary (Bitdiddle Ben) ?salary1)
     (salary ?name ?salary2)
     (lisp-value > ?salary1 ?salary2))

;; All those people supervised by someone not working in computer division.
(and (supervisor ?supervisor-name ?employee-name)
     (not (job ?supervisor-name (computer ?supervisor-job))))
