;;; syntax.scm --- Scheme Syntax Library

;;; Commentary:

;; This is an implementation of Scheme Syntax library which can be used to write
;; evaluators and compilers for Scheme-like languages.

;;; Code:

(define-module (sicp ch-4 syntax)
  :replace (self-evaluating?
	    variable?)
  :export (make-quoted
	   make-procedure
	   make-let

	   text-of-quotation
	   operator
	   operands
	   definition-variable
	   definition-value
	   assignment-variable
	   assignment-value
	   lambda-parameters
	   lambda-body
	   if-predicate
	   if-consequent
	   if-alternative
	   begin-actions
	   first-exp
	   rest-exps
	   first-operand
	   rest-operands
	   procedure-parameters
	   procedure-body
	   procedure-environment
	   let-varlist
	   let-body
	   unbind-variable-name

	   true?
	   false?
	   quoted?
	   no-operands?
	   last-operand?
	   primitive-procedure?
	   compound-procedure?
	   application?
	   last-exp?
	   tagged-list?

	   define-variable!
	   set-variable-value!
	   unbind-variable!
	   lexical-address-set!

	   cond->if
	   let->combination
	   let*->nested-let
	   letrec->let
	   while->combination

	   scan-out-defines
	   lookup-variable-value
	   lexical-address-lookup
	   extend-environment
	   apply-primitive-procedure

	   primitive-procedure-names
	   primitive-procedure-objects
	   the-empty-environment))

;; List of primitive procedures
(define primitive-procedures
  `((cons ,cons)
    (list ,list)
    (append ,append)

    (car ,car)
    (cdr ,cdr)
    (cadr ,cadr)
    (cddr ,cddr)
    (assoc ,assoc)
    (memq ,memq)
    (member ,member)
    (length ,length)

    (not ,not)
    (null? ,null?)
    (eq? ,eq?)
    (equal? ,equal?)
    (= ,=)

    (1+ ,1+)
    (1- ,1-)
    (+ ,+)
    (- ,-)
    (* ,*)
    (/ ,/)
    (remainder ,remainder)
    (abs ,abs)
    (> ,>)
    (< ,<)
    (<= ,<=)
    (>= ,>=)

    (display ,display)))



;;; Constructors

(define (make-quoted exp)
  (list 'quote exp))

;; Make a lambda expression with formal paramater PARAMATER and body BODY.
(define (make-define parameters body)
  (cons 'define (cons parameters body)))

;; Make a lambda expression with formal paramater PARAMATER and body BODY.
(define (make-lambda parameters body)
  (cons 'lambda (cons parameters body)))

;; Make a if expression with predicate PREDICATE, consequent CONSEQUENT
;; and alternative ALTERNATIVE.
(define (make-if predicate consequent alternative)
  (list 'if predicate consequent alternative))

;; Make a begin expression with sequence of expression SEQ.
(define (make-begin seq) (cons 'begin seq))

(define (make-let var-list body)
  (list 'let var-list body))

(define (make-set! var body)
  (list 'set! var body))

;; Scan-out all defines in procedure body PROC-BODY.
;;
;; Transform a procedure body from:
;;
;; (lambda (vars)
;;   (define u <e1>)
;;   (define v <e2>)
;;   <e3>)
;;
;; into:
;; (lambda (vars)
;;   (let ((u '*unassigned*)
;;	   (v '*unassigned*))
;;     (set! u <e1>)
;;     (set! v <e2>)
;;     <e3>))
;;
;; This will make our evaluator to treat all internally defined names in a truly
;; simultaneous scope -- in a block structure, the scope of all local names is
;; the entire procedure body in which the `define' is evaluated.
(define (scan-out-defines proc-body)
  ;; Transform define expression EXP with variables VARS and values VALUES into
  ;; a let expression.
  (define (define->let vars values exp)
    (if (or (null? vars) (null? values))
	exp
	(define->let
	 (cdr vars)
	 (cdr values)
	 (let-append (car vars) (car values) exp))))

  ;; Scan procedure body BODY for `defines' and accumulate them in VARS, VALUES
  ;; and FINAL-BODY.
  (define (scan body vars values final-body)
    (if (null? body)
	(list vars values (reverse final-body)) ; O(2n) is atrocious
	(let ((first (car body))
	      (rest (cdr body)))
	  (if (not (definition? (car body)))
	      (scan rest vars values (cons first final-body))
	      (scan rest
		    (cons (definition-variable first) vars)
		    (cons (definition-value first) values)
		    final-body)))))

  (let* ((tmp-list (scan proc-body '() '() '()))
	(vars (car tmp-list))
	(values (cadr tmp-list))
	(final-body (caddr tmp-list)))
    (if (and (null? vars) (null? values))
	final-body
	;; Apply accepts a list.  This bug was found by Oavis at
	;; <https://www.inchmeal.io/sicp/ch-4/ex-4.16>
	(list (append (define->let vars values '()) final-body)))))

;; Make a procedure expressin with formal parameter PARAMETER, body BODY in
;; environment ENV.
(define (make-procedure parameters body env)
  (list 'procedure parameters (scan-out-defines body) env))

(define (make-environment frame)
  (cons '*env* frame))

(define (make-frame variables values)
  (map cons variables values))



;;; Selectors

;; Syntax for quotation: (quote <text-of-quotation>)

;; Return text-of-quotation in quotation expression EXP.
(define (text-of-quotation exp) (cadr exp))

;; Syntax for assignment: (set! <var> <value>)

;; Return variable in assignment expression EXP.
(define (assignment-variable exp) (cadr exp))

;; Return value in assignment expression EXP.
(define (assignment-value exp) (caddr exp))

;; Syntax for definition: (define <var> <value>)

;; Return variable in definition expression EXP.
(define (definition-variable exp)
  (if (symbol? (cadr exp))
      (cadr exp)			; symbol name
      (caadr exp)))			; procedure name

;; Return value in definition expression EXP.
(define (definition-value exp)
  (if (symbol? (cadr exp))
      (caddr exp)
      (make-lambda (cdadr exp)		; formal parameter
                   (cddr exp))))	; body

;; Syntax for lambda: (lambda (<parameter_1> ... <parameter_n>) <body>)

;; Return parameter of lambda expression EXP.
(define (lambda-parameters exp) (cadr exp))

;; Return body of lambda expression EXP.
(define (lambda-body exp) (cddr exp))

;; Syntax for let: (let ((<var_1> <exp_1>) ... (<var_n> <exp_n>)) <body>)

;; Return list of form: (<variables> <expression>) in let expression EXP.
(define (let-varlist exp)
  (cond
   ((or (null? (car exp)) (null? (caar exp)))
    (error "Syntax error -- LET-VARLIST" exp))
   (else
    ;; REVIEW 2021-09-11: Is there a way to use single `map'?
    (list (map car (car exp))
	  (map cadr (car exp))))))

;; Return body of let expression EXP.
(define (let-body exp) (cdr exp))

;; Append VAR-VALUE pair into let expression EXP.
(define (let-append var value exp)
  (if (null? exp)
      (make-let (list (list var (make-quoted '*unassigned*)))
		(make-set! var value))
      (append
       (list 'let
	     (cons (list var (make-quoted '*unassigned*)) (cadr exp)))
       (list (make-set! var value))
       (cddr exp))))

;; Syntax for if: (if <predicate> <consequent> <alternative>)

;; Return predicate of if expression EXP.
(define (if-predicate exp) (cadr exp))

;; Return consequent of if expression EXP.
(define (if-consequent exp) (caddr exp))

;; Return alternative of if expression EXP.
(define (if-alternative exp)
  (if (not (null? (cdddr exp)))
      (cadddr exp)
      #f))

;; Syntax for begin: (begin <seq_1> <seq_2> ... <seq_n>)

;; Return list of expressions in begin expression EXP.
(define (begin-actions exp) (cdr exp))

;; Return first expression of begin sequence SEQ.
(define (first-exp seq) (car seq))

;; Return rest of the expressions of begin sequence SEQ.
(define (rest-exps seq) (cdr seq))

;; Syntax of a procedure application: (<operator> <operand_1> ... <operand_n>)

;; Return operator of procedure application expression EXP.
(define (operator exp) (car exp))

;; Return operands of procedure application expression EXP.
(define (operands exp) (cdr exp))

;; Return first operand in operand list OPS.
(define (first-operand ops) (car ops))

;; Return rest of the operand in operator list OPS.
(define (rest-operands ops) (cdr ops))

;; Syntax of a cond expression: (cond ((predicate_1 body_1) ... (predicate_n body_n)))

;; Return clauses of cond expression EXP.
(define (cond-clauses exp) (cdr exp))

;; Return predicate of cond clause CLAUSE.
(define (cond-predicate clause) (car clause))

;; Return predicate of extended cond clause CLAUSE.
(define (cond-extended-predicate clause) (car clause))

;; Return action sequence of cond clause CLAUSE.
(define (cond-actions clause) (cdr clause))

;; Return action sequence of extended clause CLAUSE.
(define (cond-extended-action clause) (caddr clause))

(define (procedure-parameters p) (cadr p))

(define (procedure-body p) (caddr p))

(define (procedure-environment p) (cadddr p))

(define (enclosing-environment env) (cdr env))

;; Return the first frame of environment ENV.
(define (first-frame env)
  (if (eq? (car env) '*env*)
      (cadr env)
      (car env)))

;; Return the rest of the frames of environment ENV.
(define (rest-frame env)
  (if (eq? (car env) '*env*)
      (cddr env)
      (cdr env)))

;; Return frame number (i.e. how many frames to pass over) for lexical address
;; LEXICAL-ADDRESS.
(define (frame-number lexical-address)
  (car lexical-address))

;; Return displacement number (i.e. how many variables to pass over in a frame)
;; for lexical address LEXICAL-ADDRESS.
(define (displacement-number lexical-address)
  (cdr lexical-address))

(define (primitive-implementation proc) (cadr proc))

(define (while-predicate exp) (car exp))

(define (while-body exp) (cdr exp))



;;; Predicates

(define (self-evaluating? exp)
  (cond
   ((boolean? exp) #t)
   ((number? exp) #t)
   ((string? exp) #t)
   (else #f)))

(define (quoted? exp)
  (tagged-list? exp 'quote))

(define (tagged-list? exp tag)
  (if (pair? exp)
      (eq? (car exp) tag)
      #f))

(define (variable? exp) (symbol? exp))

(define (assignment? exp)
  (tagged-list? exp 'set!))

(define (definition? exp)
  (tagged-list? exp 'define))

(define (lambda? exp) (tagged-list? exp 'lambda))

(define (if? exp) (tagged-list? exp 'if))

(define (begin? exp) (tagged-list? exp 'begin))

(define (last-exp? seq) (null? (cdr seq)))

(define (application? exp) (pair? exp))

(define (no-operands? ops) (null? ops))

(define (last-operand? ops)
  (null? (cdr ops)))

(define (cond? exp) (tagged-list? exp 'cond))

(define (cond-else-clause? clause)
  (eq? (cond-predicate clause) 'else))

(define (cond-extended-syntax? clause)
  (and (pair? clause)
       (= (length clause) 3)
       (eq? (cadr clause) '=>)))

(define (let-named-syntax? exp)
  (not (pair? (car exp))))

(define (true? x)
  (not (eq? x #f)))

(define (false? x)
  (eq? x #f))

(define (compound-procedure? p)
  (tagged-list? p 'procedure))

(define (primitive-procedure? proc)
  (tagged-list? proc 'primitive))

(define (let? exp) (tagged-list? exp 'let))

;; Return name of the variable in unbind! expression EXP.
(define (unbind-variable-name exp) (cadr exp))



;;; Mutators

;; Set variable VAR to value VAL in environment ENV.
(define (set-variable-value! var val env)
  (cond
   ((eq? env empty-frame)
    (error "Unbound variable -- SET-VARIABLE-VALUE!" var))
   ((assoc var (first-frame env))
    => (lambda (binding) (set-cdr! binding val)))
   (else
    (set-variable-value! var val (rest-frame env)))))

;; Define variable VAR to value VAL in environment ENV.
(define (define-variable! var val env)
  (set-car! (cdr env)
	    (assoc-set! (first-frame env) var val)))

(define (lexical-address-set! address val env)
  "Set variable at lexical address ADDRESS to value VAL in environment ENV."
  (let ((fnum (frame-number address))
	(dnum (displacement-number address)))
    (define (rec-frame count frame)
      (cond
       ((null? frame)
	(error "Unbound address -- LEXICAL-ADDRESS-SET!" address))
       ((= count 0)
	(let ((binding (car frame)))
	  (set-cdr! binding val)))
       (else (rec-frame (1- count) (cdr frame)))))

    (define (rec-env count env)
      (cond
       ((eq? env empty-frame)
	(error "Unbound address -- LEXICAL-ADDRESS-SET!" address))
       ((= count 0)
	(rec-frame dnum (first-frame env)))
       (else (rec-env (1- count) (rest-frame env)))))

    (rec-env fnum env)))

;; Syntax for 'unbind!': (unbind! <symbol>)

;; `Unbind!' name was coined by Oavis at:
;; <https://www.inchmeal.io/sicp/ch-4/ex-4.15.html>
(define (unbind-variable! var env)
  (set-car! (cdr env)
	    (assoc-remove! (first-frame env) var)))

;;; Apply

;; Used in-case of applying primitive Scheme procedures.
(define apply-in-underlying-scheme apply)

(define (apply-primitive-procedure proc args)
  (apply-in-underlying-scheme
   (primitive-implementation proc) args))



;;; General Procedures

;; Transform sequence of expression SEQ into single expression using `begin' if
;; necessary.
(define (sequence->exp seq)
  (cond ((null? seq) seq)
        ((last-exp? seq) (first-exp seq))
        (else (make-begin seq))))

(define (expand-clauses clauses)
  (if (null? clauses)
      #f                          ; no else clause
      (let ((first (car clauses))
	    (rest (cdr clauses)))
	(cond
	 ((cond-else-clause? first)
	  (if (null? rest)
	      (sequence->exp (cond-actions first))
              (error "ELSE clause isn't last -- COND->IF"
                     clauses)))
	 ((cond-extended-syntax? first)
	  (make-if (cond-extended-predicate first)
		   (list (cond-extended-action first)
			 (cond-extended-predicate first))
		   (expand-clauses rest)))
	 (else
	  (make-if (cond-predicate first)
		   (sequence->exp (cond-actions first))
		   (expand-clauses rest)))))))

;; Tranform a cond expression EXP into if expression.
(define (cond->if exp)
  (expand-clauses (cond-clauses exp)))

;; Return a environment by adding variables and values to environment BASE-ENV.
(define (extend-environment variables values base-env)
  (make-environment
    (cons (make-frame variables values) (cdr base-env))))

;; Lookup value of variable VAR in ENV.
(define (lookup-variable-value var env)
  (cond
   ((eq? env empty-frame)
    (error "Unbound variable -- LOOKUP-VARIABLE-VALUE" var))
   ((assoc var (first-frame env))
    => (lambda (binding)
	 (define var (car binding))
	 (define val (cdr binding))
	 (if (not (eq? val '*unassigned*))
	     val
	     (error "Unassigned variable -- LOOKUP-VARIABLE-VALUE"
		    var))))
   (else
    (lookup-variable-value var (rest-frame env)))))

(define (lexical-address-lookup address env)
  "Lookup variable as lexical address ADDRESS in environment ENV."
  (let ((fnum (frame-number address))
	(dnum (displacement-number address)))
    (define (rec-frame count frame)
      (cond
       ((null? frame)
	(error "Unbound address -- LEXICAL-ADDRESS-LOOKUP" address))
       ((= count 0)
	(let* ((binding (car frame))
	       (var (car binding))
	       (val (cdr binding)))
	  (if (not (eq? val '*unassigned*))
	      val
	      (error "Unassigned variable -- LEXICAL-ADDRESS-LOOKUP" var))))
       (else (rec-frame (1- count) (cdr frame)))))

    (define (rec-env count env)
      (cond
       ((eq? env empty-frame)
	(error "Unbound address -- LEXICAL-ADDRESS-LOOKUP" address))
       ((= count 0)
	(rec-frame dnum (first-frame env)))
       (else (rec-env (1- count) (rest-frame env)))))

    (rec-env fnum env)))

(define (primitive-procedure-names)
  (map car primitive-procedures))

(define (primitive-procedure-objects)
  (map (lambda (proc) (list 'primitive (cadr proc)))
       primitive-procedures))

;; By Eli: <https://eli.thegreenplace.net/2007/12/06/sicp-sections-411-412>,
;; earlier I was using a hand-made iterative process.

;; Tranform let expression EXP into a combination.
(define (let->combination exp)
  ;; TODO 2021-09-11: Cleanup!
  (if (let-named-syntax? exp)
      (let* ((name (car exp))
	     (var-list (let-varlist (cdr exp)))
	     (var (car var-list))
	     (expr (cadr var-list)))
	(sequence->exp
	 (list (make-define (cons name var) (let-body (cdr exp)))
	       (cons name expr))))
      (let* ((var-list (let-varlist exp))
	     (var (car var-list))
	     (expr (cadr var-list)))
	(cons (make-lambda var (let-body exp)) expr))))

;; Transform let* expression EXP into nested lets
(define (let*->nested-let exp)
  (define (rec var-list body)
    (if (null? var-list)
	body
	(make-let (list (car var-list)) (rec (cdr var-list) body))))
  (rec (car exp)
       (cadr exp)))

(define (letrec->let exp)
  (define (iter vars values exp)
    (if (or (null? vars) (null? values))
	exp
	(iter (cdr vars)
	      (cdr values)
	      (let-append (car vars) (car values) exp))))
  (let ((varlist (let-varlist exp)))
    (append (iter (car varlist) (cadr varlist) '())
	    (cdr exp))))

;; Syntax for while: (while <predicate> <body>)

;; Transform a `while' expression EXP into `if' expression.
(define (while->combination exp)
  (sequence->exp
   (list
    (make-define
     (list 'iter)
     (list (make-if
	    (while-predicate exp)
	    (sequence->exp (append (while-body exp) '((iter))))
	    #f)))
    '(iter))))

;;; Environment

(define empty-frame (make-frame '() '()))

(define the-empty-environment (make-environment empty-frame))

;;; syntax.scm ends here
