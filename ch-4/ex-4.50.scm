;;; Random AMB implementation.

;; TODO 2021-09-19: Solve part 2.

;; Analyze an AMB expression exp
(define (analyze-amb exp)
  (let* ((cprocs (map analyze (amb-choices exp)))
	 (count (length cprocs)))
    (lambda (env succeed fail)
      (define (try-next choices count)
        (if (null? choices)
            (fail)
	    (let ((choice (list-ref choices (random 0 count))))
              (choice
	       env
               succeed
               (lambda ()
		 (try-next (delete choice choices) (1- count)))))))
      (try-next cprocs count))))
