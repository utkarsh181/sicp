#+title: SICP Exercise 4.60
#+author: Utkarsh Singh

* Problem

#+begin_src scheme
(rule (lives-near ?person-1 ?person-2)
      (and (address ?person-1 (?town . ?rest-1))
	   (address ?person-2 (?town . ?rest-2))
	   (not (same ?person-1 ?person-2))))
#+end_src

* Solution

Taking the example given in this exercise.  In first run, database is
checked for all the people living near Alyssa P. Hacker.  Then, in
second run, database in again checked for all the people living near
Cy D Fect, which explains the reason for duplicate =live-near=
results.
