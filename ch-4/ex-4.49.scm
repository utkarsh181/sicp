;;; Natural Language parsing

(define nouns '(noun student professor cat class))
(define verbs '(verb studies lectures eats sleeps))
(define articles '(article the a))
(define prepositions '(prep for to in by with))

(define *unparsed* '())

;; NOTE 2021-09-22: By Oavis: <https://www.inchmeal.io/sicp/ch-4/ex-4.49>
(define (list-amb lst)
  (require (not (null? lst)))
  (amb (car lst) (list-amb (cdr lst))))

;; Parse words in list WORD-LIST.
(define (parse-word word-list)
  (require (not (null? *unparsed*)))
  (let ((found-word (list-amb (cdr *unparsed*))))
    (set! *unparsed* (cdr *unparsed*))
    (list (car word-list) found-word)))

(define (parse-simple-noun-phrase)
  (list 'simple-noun-phrase
	(parse-word articles)
	(parse-word nouns)))

(define (maybe-extend-noun noun-phrase)
  (amb noun-phrase
       (maybe-extend-noun
	(list 'noun-phrase
	      noun-phrase
	      (parse-prepositional-phrase)))))

(define (parse-noun-phrase)
  (maybe-extend-noun (parse-simple-noun-phrase)))

(define (parse-prepositional-phrase)
  (list 'prep-phrase
	(parse-word prepositions)
	(parse-noun-phrase)))

(define (maybe-extend-verb verb-phrase)
  (amb verb-phrase
       (maybe-extend-verb
	(list 'verb-phrase
	      verb-phrase
	      (parse-prepositional-phrase)))))

(define (parse-verb-phrase)
  (maybe-extend-verb (parse-word verbs)))

(define (parse-sentence)
  (list 'sentence
	(parse-noun-phrase)
	(parse-verb-phrase)))

;; Parse input list INPUT.
(define (parse input)
  (set! *unparsed* input)
  (let ((sent (parse-sentence)))
    (require (null? *unparsed*)) sent))
