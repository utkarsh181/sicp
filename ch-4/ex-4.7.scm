;;; Prototype for `let*'.  Also see "beval.scm".

(define (let*->nested-let exp)
  (define (rec var-list body)
    (if (null? var-list)
	body
	(make-let (list (car var-list)) (rec (cdr var-list) body))))
  (rec (car (operands exp))
       (cadr (operands exp))))
