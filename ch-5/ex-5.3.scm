;; Controller instruction sequence for Sqrt machine.

(use-modules (sicp ch-5 simulator))

(define (square x) (* x x))

(define (average a b) (/ (+ a b) 2))

(define sqrt-operation-table
  (append `((square ,square) (average ,average))
	  operation-table))

(define sqrt-machine
  (make-machine
   '(num guess t)
   sqrt-operation-table
   '((assign guess (const 1.0))

     test-guess

     good-enough?
     (assign t (op square) (reg guess))
     (assign t (op -) (reg t) (reg num))
     (assign t (op abs) (reg t))
     (test (op <) (reg t) (const 0.01))
     (branch (label sqrt-done))
     good-enough?-done

     improve
     (assign t (op /) (reg num) (reg guess))
     (assign guess (op average) (reg guess) (reg t))
     (goto (label test-guess))
     improve-done

     sqrt-done)))
