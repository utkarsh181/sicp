;; Controller instruction sequence for Factorial register machine

(use-modules (sicp ch-5 simulator))

(define factorial-machine
  (make-machine
   '(n continue val)
   operation-table
   '((assign continue (label fact-done))

     fact-loop
     (test (op =) (reg n) (const 1))
     (branch (label base-case))
     (save continue)
     (save n)
     (assign n (op -) (reg n) (const 1))
     (assign continue (label after-fact))
     (goto (label fact-loop))

     after-fact
     (restore n)
     (restore continue)
     (assign val (op *) (reg n) (reg val))
     (goto (reg continue))

     base-case
     (assign val (const 1))
     (goto (reg continue))

     fact-done)))

(define (factorial x)
  (set-register-contents! factorial-machine 'n x)
  (simulator-start factorial-machine)
  (get-register-contents factorial-machine 'val))
