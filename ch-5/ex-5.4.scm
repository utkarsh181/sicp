;; Controller instruction sequence for Expt machine.

(use-modules (sicp ch-5 simulator))

;;; Recurive process

(define expt-rec-machine
  (make-machine
   '(base power result continue)
   operation-table
   '((assign continue (label expt-done))

     expt-loop
     (test (op =) (reg power) (const 0))
     (branch (label base-case))
     (save continue)
     (assign continue (label after-expt))
     (assign power (op -) (reg power) (const 1))
     (goto (label expt-loop))

     after-expt
     (restore continue)
     (assign result (op *) (reg result) (reg base))
     (goto (reg continue))

     base-case
     (assign result (const 1))
     (goto (reg continue))

     expt-done)))

;;; Iterative process

(define expt-iter-machine
  (make-machine
   '(base power counter result)
   operation-table
   '((assign counter (reg power))
     (assign result (const 1))

     expt-iter
     (test (op =) (reg counter) (const 0))
     (branch (label expt-done))
     (assign counter (op -) (reg counter) (const 1))
     (assign result (op *) (reg base) (reg result))
     (goto (label expt-iter))

     expt-done)))
