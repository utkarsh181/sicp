;;; eceval.scm --- Explicit Control Evaluator

;;; Commentary:

;; This program implements a Explicit control evaluator which can be used to
;; understand how procedure calling and argument-passing mechanism used in
;; evaluation process can be described in terms of operation on register and
;; stacks.  With this we can now answer questions introduced in "simulator.scm".

(define-module (sicp ch-5 eceval)
  :use-module (sicp ch-4 table)
  :use-module (sicp ch-4 syntax)
  :use-module (sicp ch-5 simulator)
  :use-module (sicp ch-5 compiler)
  :export (eceval-machine
	   driver-loop
	   compile-and-go))

;;; Code:

;; TODO 2021-10-18: Add a procedure along the line `compile-eval', which just
;; evaluates the expression and prints it's result.

;;; REPL

(define (prompt-for-input string)
  (newline) (newline) (display string) (newline))

(define (announce-output string)
  (newline) (display string) (newline))

(define (user-print object)
  (cond
   ((compound-procedure? object)
    (display (list 'compound-procedure
                   (procedure-parameters object)
                   (procedure-body object)
                   '<procedure-env>)))
   ((compiled-procedure? object)
    (display '<compiled-procedure>))
   (else (display object))))

(define (setup-environment)
  (let ((initial-env (extend-environment
		      (primitive-procedure-names)
                      (primitive-procedure-objects)
                      the-empty-environment)))
    (define-variable! #t #t initial-env)
    (define-variable! #f #f initial-env)
    (install-eceval-package)
    initial-env))

(define the-global-environment '())

(define (get-global-environment) the-global-environment)

;;; Register Machine

(define (install-eceval-package)
  (put 'eceval 'quote 'ev-quoted)
  (put 'eceval 'set! 'ev-assignment)
  (put 'eceval 'define 'ev-definition)
  (put 'eceval 'if 'ev-if)
  (put 'eceval 'lambda 'ev-lambda)
  (put 'eceval 'begin 'ev-begin)
  (put 'eceval 'cond 'ev-cond)
  (put 'eceval 'let 'ev-let)
  (put 'eceval 'let* 'ev-let*)
  (put 'eceval 'letrec 'ev-letrec))

(define (empty-arglist) '())

(define (adjoin-arg arg arglist)
  (append arglist (list arg)))

(define eceval-operation-table
  `((read ,read)

    (put ,put)
    (get ,get)

    (cons ,cons)
    (list ,list)

    (make-procedure ,make-procedure)
    (make-compiled-procedure ,make-compiled-procedure)

    (definition-variable ,definition-variable)
    (definition-value ,definition-value)
    (assignment-variable ,assignment-variable)
    (assignment-value ,assignment-value)
    (operator ,operator)
    (operands ,operands)
    (lambda-parameters ,lambda-parameters)
    (lambda-body ,lambda-body)
    (if-predicate ,if-predicate)
    (if-consequent ,if-consequent)
    (if-alternative ,if-alternative)
    (begin-actions ,begin-actions)
    (first-exp ,first-exp)
    (rest-exps ,rest-exps)
    (first-operand ,first-operand)
    (rest-operands ,rest-operands)
    (empty-arglist ,empty-arglist)
    (procedure-parameters ,procedure-parameters)
    (procedure-body ,procedure-body)
    (procedure-environment ,procedure-environment)
    (get-global-environment ,get-global-environment)
    (compiled-procedure-entry ,compiled-procedure-entry)
    (compiled-procedure-env ,compiled-procedure-env)

    (true? ,true?)
    (false? ,false?)
    (self-evaluating? ,self-evaluating?)
    (quoted? ,quoted?)
    (text-of-quotation ,text-of-quotation)
    (variable? ,variable?)
    (last-exp? ,last-exp?)
    (application? ,application?)
    (no-operands? ,no-operands?)
    (last-operand? ,last-operand?)
    (primitive-procedure? ,primitive-procedure?)
    (compound-procedure? ,compound-procedure?)
    (compiled-procedure? ,compiled-procedure?)

    (define-variable! ,define-variable!)
    (set-variable-value! ,set-variable-value!)

    (apply-primitive-procedure ,apply-primitive-procedure)
    (lookup-variable-value ,lookup-variable-value)
    (extend-environment ,extend-environment)
    (adjoin-arg ,adjoin-arg)
    (cond->if ,cond->if)
    (let->combination ,let->combination)
    (let*->nested-let ,let*->nested-let)
    (letrec->let ,letrec->let)
    (while->combination ,while->combination)
    (prompt-for-input ,prompt-for-input)
    (announce-output ,announce-output)
    (user-print ,user-print)))

(define eceval-machine
  (make-machine
   '(exp env val proc argl continue unev compapp)
   eceval-operation-table
   '((assign compapp (label compound-apply))
     (branch (label external-entry))	; It is controlled by an external flag.

     read-eval-print-loop
     (perform (op stack-initialize))
     (perform (op prompt-for-input) (const ";;; EC-Eval Input:"))
     (assign exp (op read))
     (assign env (op get-global-environment))
     (assign continue (label print-result))
     (goto (label eval-dispatch))

     print-result
     (perform (op announce-output) (const ";;; EC-Eval Value:"))
     (perform (op user-print) (reg val))
     (goto (label read-eval-print-loop))

     external-entry
     (perform (op stack-initialize))
     (assign env (op get-global-environment))
     (assign continue (label print-result))
     (goto (reg val))

     unknown-expression-type
     (assign val (const unknown-expression-type-error))
     (goto (label signal-error))

     unknown-procedure-type
     (restore continue)
     (assign val (const unknown-procedure-type-error))
     (goto (label signal-error))

     signal-error
     (perform (op user-print) (reg val))
     (goto (label read-eval-print-loop))

     eval-dispatch
     (test (op self-evaluating?) (reg exp))
     (branch (label ev-self-eval))
     (test (op variable?) (reg exp))
     (branch (label ev-variable))

     ;; Search for valid `label' for EXP in data-directed manner.
     (assign unev (op operator) (reg exp))
     (assign unev (op get) (const eceval) (reg unev)) ;label
     (test (op true?) (reg unev))
     (branch (reg unev))

     (test (op application?) (reg exp))
     (branch (label ev-application))
     (goto (label unknown-expression-type))

     ev-self-eval
     (assign val (reg exp))
     (goto (reg continue))

     ev-variable
     (assign val (op lookup-variable-value) (reg exp) (reg env))
     (goto (reg continue))

     ev-quoted
     (assign val (op text-of-quotation) (reg exp))
     (goto (reg continue))

     ev-lambda
     (assign unev (op lambda-parameters) (reg exp))
     (assign exp (op lambda-body) (reg exp))
     (assign val (op make-procedure) (reg unev) (reg exp) (reg env))
     (goto (reg continue))

     ;; Evaluation procedure application expression EXP.
     ev-application
     (save continue)
     (assign unev (op operands) (reg exp))
     (assign exp (op operator) (reg exp))
     (test (op variable?) (reg exp))
     (branch (label ev-appl-variable-operator))
     (save env)
     (save unev)
     (assign continue (label ev-appl-restore-operator-env))
     (goto (label eval-dispatch))

     ;; Evaluate a symbold operator expression EXP.
     ev-appl-variable-operator
     (assign continue (label ev-appl-did-operator))
     (goto (label eval-dispatch))

     ev-appl-restore-operator-env
     (restore unev)			; operators
     (restore env)

     ;; Setup register after evaluating operator in procedure application EXP.
     ev-appl-did-operator
     (assign argl (op empty-arglist))
     (assign proc (reg val))		; operands
     (test (op no-operands?) (reg unev))
     (branch (label apply-dispatch))
     (save proc)

     ;; Evaluate operands in stack UNEV.
     ev-appl-operand-loop
     (save argl)
     (assign exp (op first-operand) (reg unev))
     (test (op last-operand?) (reg unev))
     (branch (label ev-appl-last-arg))
     (test (op self-evaluating?) (reg exp))
     (branch (label ev-appl-self-eval-operand))
     (save env)
     (save unev)
     (assign continue (label ev-appl-restore-operand-env))
     (goto (label eval-dispatch))

     ev-appl-self-eval-operand
     (assign continue (label ev-appl-accumulate-arg))
     (goto (label eval-dispatch))

     ev-appl-restore-operand-env
     (restore unev)
     (restore env)

     ;; Accumulate evaluated argument in argument list ARGL.
     ev-appl-accumulate-arg
     (restore argl)
     (assign argl (op adjoin-arg) (reg val) (reg argl))
     (assign unev (op rest-operands) (reg unev))
     (goto (label ev-appl-operand-loop))

     ;; Evaluate last operand of procedure application expression EXP.
     ev-appl-last-arg
     (assign continue (label ev-appl-accum-last-arg))
     (goto (label eval-dispatch))

     ev-appl-accum-last-arg
     (restore argl)
     (assign argl (op adjoin-arg) (reg val) (reg argl))
     (restore proc)
     (goto (label apply-dispatch))

     ;; Dispatch on procedure type PROC.
     apply-dispatch
     (test (op primitive-procedure?) (reg proc))
     (branch (label primitive-apply))
     (test (op compound-procedure?) (reg proc))
     (branch (label compound-apply))
     (test (op compiled-procedure?) (reg proc))
     (branch (label compiled-apply))
     (goto (label unknown-procedure-type))

     compiled-apply
     (restore continue)
     (assign val (op compiled-procedure-entry) (reg proc))
     (goto (reg val))

     ;; Apply primtive procedure PROC on argument list ARGL and store value in
     ;; register VAL.
     primitive-apply
     (assign val (op apply-primitive-procedure) (reg proc) (reg argl))
     (restore continue)
     (goto (reg continue))

     ;; Apply compund procedure PROC on argument list ARGL by extending its
     ;; enviornment and then evaluating its body.
     compound-apply
     (assign unev (op procedure-parameters) (reg proc))
     (assign env (op procedure-environment) (reg proc))
     (assign env (op extend-environment) (reg unev) (reg argl) (reg env))
     (assign unev (op procedure-body) (reg proc))
     (goto (label ev-sequence))

     ;; Evaluate begin expression EXP.
     ev-begin
     (assign unev (op begin-actions) (reg exp))
     (save continue)
     (goto (label ev-sequence))

     ;; Evaluate sequence of expression EXP.
     ev-sequence
     (assign exp (op first-exp) (reg unev))
     (test (op last-exp?) (reg unev))
     (branch (label ev-sequence-last-exp))
     (save unev)
     (save env)
     (assign continue (label ev-sequence-continue))
     (goto (label eval-dispatch))

     ;; Update register UNEV after evaluating 'a' expression in sequence.
     ev-sequence-continue
     (restore env)
     (restore unev)
     (assign unev (op rest-exps) (reg unev))
     (goto (label ev-sequence))

     ;; Evaluate last expression in sequence of begin expression.
     ev-sequence-last-exp
     ;; NOTE 2021-10-12: We are not saving any information here.  Yes! this is a
     ;; tail-recursive evaluator.  Also see SICP Section 5.4 (Tail recursion).
     (restore continue)
     (goto (label eval-dispatch))

     ev-if
     (save exp)
     (save env)
     (save continue)
     (assign continue (label ev-if-decide))
     (assign exp (op if-predicate) (reg exp))
     (goto (label eval-dispatch))

     ev-if-decide
     (restore continue)
     (restore env)
     (restore exp)
     (test (op true?) (reg val))
     (branch (label ev-if-consequent))

     ev-if-alternative
     (assign exp (op if-alternative) (reg exp))
     (goto (label eval-dispatch))

     ev-if-consequent
     (assign exp (op if-consequent) (reg exp))
     (goto (label eval-dispatch))

     ev-assignment
     (assign unev (op assignment-variable) (reg exp))
     (save unev)
     (assign exp (op assignment-value) (reg exp))
     (save env)
     (save continue)
     (assign continue (label ev-assignment-1))
     (goto (label eval-dispatch))

     ev-assignment-1
     (restore continue)
     (restore env)
     (restore unev)
     (perform (op set-variable-value!) (reg unev) (reg val) (reg env))
     (assign val (const ok))
     (goto (reg continue))

     ev-definition
     (assign unev (op definition-variable) (reg exp))
     (save unev)
     (assign exp (op definition-value) (reg exp))
     (save env)
     (save continue)
     (assign continue (label ev-definition-1))
     (goto (label eval-dispatch))

     ev-definition-1
     (restore continue)
     (restore env)
     (restore unev)
     (perform (op define-variable!) (reg unev) (reg val) (reg env))
     (assign val (const ok))
     (goto (reg continue))

     ev-cond
     (assign exp (op cond->if) (reg exp))
     (goto (label eval-dispatch))

     ev-let
     (assign unev (op operands) (reg exp))
     (assign exp (op let->combination) (reg unev))
     (goto (label eval-dispatch))

     ev-let*
     (assign unev (op operands) (reg exp))
     (assign exp (op let*->nested-let) (reg unev))
     (goto (label eval-dispatch))

     ev-letrec
     (assign unev (op operands) (reg exp))
     (assign exp (op letrec->let) (reg unev))
     (goto (label eval-dispatch)))))

(define (driver-loop)
  (set! the-global-environment (setup-environment))
  (set-register-contents! eceval-machine 'flag #f)
  (simulator-start eceval-machine))

(define (compile-and-go exp)
  (let ((instructions
         (assemble (statements (compile exp 'val 'return))
                   eceval-machine)))
    (set! the-global-environment (setup-environment))
    (set-register-contents! eceval-machine 'val instructions)
    (set-register-contents! eceval-machine 'flag #t)
    (simulator-start eceval-machine)))

;;; eceval.scm ends here
