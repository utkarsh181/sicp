;; Controller instruction sequence for Fibonacci machine.

(use-modules (sicp ch-5 simulator))

;; NOTE 2021-10-05: This Fibonacci series starts from zero.
(define fib-machine
  (make-machine
   '(n continue result)
   operation-table
   '((assign continue (label fib-done))

     fib-loop
     (test (op <) (reg n) (const 2))
     (branch (label immediate-answer))
     (save continue)
     (assign continue (label afterfib-n-1))
     (save n)
     (assign n (op -) (reg n) (const 1))
     (goto (label fib-loop))

     afterfib-n-1
     (restore n)
     (assign n (op -) (reg n) (const 2))
     (assign continue (label afterfib-n-2))
     (save result)				;save Fib(n-1)
     (goto (label fib-loop))

     afterfib-n-2
     (restore n)			;n contains Fib(n-1)
     (restore continue)
     (assign result (op +) (reg result) (reg n))
     (goto (reg continue))

     immediate-answer
     (assign result (reg n))
     (goto (reg continue))
     fib-done)))

(define (fib x)
  (set-register-contents! fib-machine 'n x)
  (simulator-start fib-machine)
  (get-register-contents fib-machine 'result))
