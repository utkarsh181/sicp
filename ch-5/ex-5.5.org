#+title: SICP Exercise 5.5
#+author: Utkarsh Singh

For this exercise I will try to simulate =(fib 3)= as it contains
exactly two recursive call:

#+begin_src fundamental
(fib 3)

;; In fib-loop
stack: (2 (label afterfib-n-1) 3 (label afterfib-n-1) (label fib-done))
register: ((n . 1) (continue . (label afterfib-n-1)))

;; In immediate-answer
stack: (2 (label afterfib-n-1) 3 (label afterfib-n-1) (label fib-done))
register: ((n . 1) (val . 1) (continue . (label afterfib-n-1)))

;; In afterfib-n-1
stack: (1 (label afterfib-n-1) 3 (label afterfib-n-1) (label fib-done))
register: ((n . 0) (val . 1) (continue . (label afterfib-n-2)))

;; In fib-loop and then in immediate answer
stack: (1 (label afterfib-n-1) 3 (label afterfib-n-1) (label fib-done))
register: ((n . 0) (val . 0) (continue . (label afterfib-n-2)))

;; In afterfib-n-2
stack: (3 (label afterfib-n-1) (label fib-done))
register: ((n . 0) (val . 1) (continue . (label afterfib-n-1)))

;; In afterfib-n-1
stack: (1 (label fib-done))
register: ((n . 1) (val . 1) (continue . (label afterfib-n-1)))

;; In immediate-answer
stack: (1 (label fib-done))
register: ((n . 1) (val . 1) (continue . (label afterfib-n-1)))

;; In afterfib-n-2
stack: ()
register: ((n . 1) (val . 2) (continue . (label fib-done)))
#+end_src
