;;; compiler.scm --- Scheme to Pseudo Assembly Compiler

;;; Commentary:

;;; Code:

(define-module (sicp ch-5 compiler)
  :use-module (srfi srfi-1)
  :use-module (sicp ch-4 table)
  :use-module (sicp ch-4 syntax)
  :export (make-compiled-procedure

	   statements
	   compiled-procedure-entry
	   compiled-procedure-env
	   compiled-procedure?

	   compile))

(define *label-counter* 0)

(define registers '(env proc val argl continue))

(define argument-registers '(arg1 arg2))

;;; Constructors

;; Make sequence of instruction with statements STATEMENTS which needs and
;; modifies register-list NEEDS and MODIFIES respectively.
(define (make-instruction-sequence needs modifies statements)
  (list needs modifies statements))

(define (make-empty-instruction-sequence)
  (make-instruction-sequence '() '() '()))

(define (new-label-number)
  (set! *label-counter* (+ 1 *label-counter*))
  *label-counter*)

(define (make-label name)
  (string->symbol
    (string-append (symbol->string name)
                   (number->string (new-label-number)))))

(define (make-lexical-address frame-number displacement-number)
  (cons frame-number displacement-number))

(define (make-compile-frame variables) variables)

(define (make-compile-environment frame)
  (cons '*cenv* frame))

(define (make-compiled-procedure entry env)
  "Return a compiled procedure starting at label ENTRY with environment ENV."
  (list 'compiled-procedure entry env))

;;; Selectors

(define (registers-needed s)
  (if (symbol? s) '() (car s)))

(define (registers-modified s)
  (if (symbol? s) '() (cadr s)))

;; Return the first frame of environment ENV.
(define (first-frame env)
  (if (eq? (car env) '*cenv*)
      (cadr env)
      (car env)))

;; Return the rest of the frames of environment ENV.
(define (rest-frame env)
  (if (eq? (car env) '*cenv*)
      (cddr env)
      (cdr env)))

(define (statements s)
  "Return statements/instructions of instruction sequence S."
  (if (symbol? s) (list s) (caddr s)))

(define (compiled-procedure-entry c-proc)
  "Return entry label for compiled procedure C-PROC."
  (cadr c-proc))

(define (compiled-procedure-env c-proc)
  "Return environment for compiled procedure C-PROC."
  (caddr c-proc))

;;; Predicates

(define (needs-register? seq reg)
  (memq reg (registers-needed seq)))

(define (modifies-register? seq reg)
  (memq reg (registers-modified seq)))

(define (compiled-procedure? proc)
  (tagged-list? proc 'compiled-procedure))

;;; General Procedures

(define (list-union s1 s2)
  (lset-union eq? s1 s2))

(define (list-difference s1 s2)
  (lset-difference eq? s1 s2))

(define (list-adjoin s1 s2)
  (if (null? s2)
      s1
      (list-adjoin (lset-adjoin eq? s1 (car s2)) (cdr s2))))

;; Append statements of list of instruction sequences SEQS and also updating
;; modified and needed registers.
(define (append-instruction-sequences . seqs)
  (define (append-2-sequences seq1 seq2)
    (make-instruction-sequence
     (list-adjoin (registers-needed seq1)
                  (registers-needed seq2))
     (list-union (registers-modified seq1)
                 (registers-modified seq2))
     (append (statements seq1) (statements seq2))))

  (define (append-seq-list seqs)
    (if (null? seqs)
        (make-empty-instruction-sequence)
        (append-2-sequences (car seqs)
                            (append-seq-list (cdr seqs)))))

  (append-seq-list seqs))

;; Return instruction sequence by combining two parallel instruction sequence
;; SEQ1 and SEQ2.
(define (parallel-instruction-sequences seq1 seq2)
  (make-instruction-sequence
   (list-union (registers-needed seq1)
               (registers-needed seq2))
   (list-union (registers-modified seq1)
               (registers-modified seq2))
   (append (statements seq1) (statements seq2))))

;; Return an instruction sequence whose statements are the statements of
;; instruction sequence SEQ1 followed by statements of instruction sequence
;; SEQ2, with appropriate `save' and `restore' instructions around SEQ1 to
;; protect the registers in register list REGS.
(define (preserving regs seq1 seq2)
  (if (null? regs)
      (append-instruction-sequences seq1 seq2)
      (let ((first-reg (car regs)))
        (if (not (and (needs-register? seq2 first-reg)
                      (modifies-register? seq1 first-reg)))
	    (preserving (cdr regs) seq1 seq2)
	    (preserving
	     (cdr regs)
	     (make-instruction-sequence
	      (list-union (list first-reg)
			  (registers-needed seq1))
	      (list-difference (registers-modified seq1)
			       (list first-reg))
	      (append `((save ,first-reg))
		      (statements seq1)
		      `((restore ,first-reg))))
	     seq2)))))

(define (compile-linkage linkage)
  (cond
   ((eq? linkage 'return)
    (make-instruction-sequence '(continue) '() '((goto (reg continue)))))
   ((eq? linkage 'next)
    (make-empty-instruction-sequence))
   (else
    (make-instruction-sequence '() '() `((goto (label ,linkage)))))))

;; End instruction sequence INSTRUCTION-SEQUENCE with linkage LINKAGE.
(define (end-with-linkage linkage instruction-sequence)
  (preserving '(continue)		; TODO 2021-10-18: Add a global variable
					; that defines register suitable for
					; linkage.
	      instruction-sequence
	      (compile-linkage linkage)))

(define (compile-self-evaluating exp target linkage env)
  (end-with-linkage
   linkage
   (make-instruction-sequence
    '() (list target)
    `((assign ,target (const ,exp))))))

(define (compile-quoted exp target linkage env)
  (end-with-linkage
   linkage
   (make-instruction-sequence
    '() (list target)
    `((assign ,target (const ,(text-of-quotation exp)))))))

(define (find-variable variable env)
  (define (rec-frame displacement-num frame frame-num env)
    (cond
     ((null? frame)
      (rec-env (1+ frame-num) (rest-frame env)))
      ((eq? variable (car frame))
       (make-lexical-address frame-num displacement-num))
     (else
      (rec-frame (1+ displacement-num) (cdr frame) frame-num env))))

  (define (rec-env frame-num env)
    (if (eq? env empty-compile-frame)
	'not-found
	(rec-frame 0 (first-frame env) frame-num env)))

  (rec-env 0 env))

;; TODO 2021-10-23: Define a custom operation that only global-environment for
;; lookup.
(define (compile-variable exp target linkage env)
  (let ((lex-address (find-variable exp env)))
    (end-with-linkage
     linkage
     (make-instruction-sequence
      '(env) (list target)
      (if (eq? lex-address 'not-found)
	  `((assign ,target
		    (op lookup-variable-value) (const ,exp) (reg env)))
	  `((assign ,target
		    (op lexical-address-lookup) (const ,lex-address) (reg env))))))))

;; Compile assignment expression EXP which targets register TARGET and linkage
;; LINKAGE.
(define (compile-assignment exp target linkage env)
  (let* ((var (assignment-variable exp))
	 (lex-address (find-variable var env))
         (value-code (compile (assignment-value exp) 'val 'next env)))
    (end-with-linkage
     linkage
     (preserving
      '(env) value-code
      (make-instruction-sequence
       '(env val) (list target)
       (if (eq? lex-address 'not-found)
	   `((perform (op set-variable-value!)
                      (const ,var)
                      (reg val)
                      (reg env))
             (assign ,target (const ok)))
	   `((perform (op lexical-address-set!)
                      (const ,var)
                      (reg val)
                      (reg env))
             (assign ,target (const ok)))))))))

;; Compile definition expression EXP which targets register TARGET and linkage
;; LINKAGE.
(define (compile-definition exp target linkage env)
  (let ((var (definition-variable exp))
        (value-code (compile (definition-value exp) 'val 'next env)))
    (end-with-linkage
     linkage
     (preserving
      '(env) value-code
      (make-instruction-sequence
       '(env val) (list target)
       `((perform (op define-variable!)
                  (const ,var)
                  (reg val)
                  (reg env))
         (assign ,target (const ok))))))))

(define (compile-if exp target linkage env)
  (let* ((t-branch (make-label 'true-branch))
         (f-branch (make-label 'false-branch))
         (after-if (make-label 'after-if))
	 (consequent-linkage (if (eq? linkage 'next) after-if linkage))
	 (predicate-code (compile (if-predicate exp) 'val 'next env))
         (consequent-code
          (compile (if-consequent exp) target consequent-linkage env))
         (alternative-code
          (compile (if-alternative exp) target linkage env)))
    (preserving
     '(env continue) predicate-code
     (append-instruction-sequences

      (make-instruction-sequence
       '(val) '()
       `((test (op false?) (reg val))
	 (branch (label ,f-branch))))

      (parallel-instruction-sequences
       (append-instruction-sequences t-branch consequent-code)
       (append-instruction-sequences f-branch alternative-code))

      after-if))))

;; Compile sequence of expression SEQ which targets register TARGET and linkage
;; LINKAGE.
(define (compile-sequence seq target linkage env)
  (if (last-exp? seq)
      (compile (first-exp seq) target linkage env)
      (preserving
       '(env continue)
       (compile (first-exp seq) target 'next env)
       (compile-sequence (rest-exps seq) target linkage env))))

;; Return sequence of instrunction by tacking sequence SEQ to body of sequence
;; BODY-SEQ.
(define (tack-on-instruction-sequence seq body-seq)
  (make-instruction-sequence
   (registers-needed seq)
   (registers-modified seq)
   (append (statements seq) (statements body-seq))))

(define (extend-compile-environment variables base-env)
  (make-compile-environment (cons variables (cdr base-env))))

(define (compile-lambda-body exp proc-entry env)
  (let ((formals (lambda-parameters exp)))
    (append-instruction-sequences
     (make-instruction-sequence
      '(env proc argl) '(env)
      `(,proc-entry
        (assign env (op compiled-procedure-env) (reg proc))
        (assign env
                (op extend-environment)
                (const ,formals)
                (reg argl)
                (reg env))))
     (compile-sequence (scan-out-defines (lambda-body exp)) 'val 'return env))))

(define (compile-lambda exp target linkage env)
  (let* ((proc-entry (make-label 'entry))
         (after-lambda (make-label 'after-lambda))
	 (lambda-linkage (if (eq? linkage 'next) after-lambda linkage)))
    (append-instruction-sequences
     (tack-on-instruction-sequence
      (end-with-linkage
       lambda-linkage
       (make-instruction-sequence
	'(env) (list target)
	`((assign ,target
		  (op make-compiled-procedure)
		  (label ,proc-entry)
		  (reg env)))))
      (compile-lambda-body
       exp
       proc-entry
       (extend-compile-environment (lambda-parameters exp) env)))
     after-lambda)))

(define (compile-begin exp target linkage env)
  (compile-sequence (begin-actions exp) target linkage env))

(define (spread-arguments operands env)
  (if (not (= 2 (length operands)))
      (error "Operands not equal to 2 -- COMPILE" operands)
      (preserving
       '(env)
       (compile (car operands) 'arg1 'next env)
       (preserving
	'(arg1)
	(compile (cadr operands) 'arg2 'next env)
	(make-instruction-sequence '(arg2) '() '())))))

;; Return object code to construct argument list (`argl') for rest of the
;; arguments (i.e. NOT last argument) object code OPERAND-CODES.
(define (code-to-get-rest-args operand-codes)
  (let ((code-for-next-arg
         (preserving
	  '(argl) (car operand-codes)
	  (make-instruction-sequence
	   '(val argl) '(argl)
	   '((assign argl (op cons) (reg val) (reg argl)))))))
    (if (null? (cdr operand-codes))
        code-for-next-arg
        (preserving '(env)
         code-for-next-arg
         (code-to-get-rest-args (cdr operand-codes))))))

;; Construct argument list (`argl') from operand's object code OPERAND-CODES.
(define (construct-arglist operand-codes)
  ;; NOTE 2021-10-20: Here are are compiling argument from right to left.
  (let ((operand-codes (reverse operand-codes)))
    (if (null? operand-codes)
        (make-instruction-sequence
	 '() '(argl)
	 '((assign argl (const ()))))
        (let ((code-to-get-last-arg
               (append-instruction-sequences
                (car operand-codes)
                (make-instruction-sequence
		 '(val) '(argl)
		 '((assign argl (op list) (reg val)))))))
          (if (null? (cdr operand-codes))
              code-to-get-last-arg
              (preserving '(env)
			  code-to-get-last-arg
			  (code-to-get-rest-args
			   (cdr operand-codes))))))))

;; Return the object code for procedure application with target register TARGET
;; and linkage LINKAGE.
(define (compile-proc-appl target linkage env)
  (cond
   ;; With this implementation of `return' linkage, the compiler generates
   ;; tail-recursive code.  Calling the procedure as the final step in procedure
   ;; body does direct transfer to `val', without saving any information on the
   ;; stack.
   ((and (eq? target 'val)
	 (not (eq? linkage 'return)))
    (make-instruction-sequence
     '(proc) registers
     `((assign continue (label ,linkage))
       (assign val (op compiled-procedure-entry) (reg proc))
       (goto (reg val)))))
   ;; Setup `continue' such that after executing procedure `proc' we will return
   ;; to `proc-return'.
   ((and (not (eq? target 'val))
         (not (eq? linkage 'return)))
    (let ((proc-return (make-label 'proc-return)))
      (make-instruction-sequence
       '(proc) registers
       `((assign continue (label ,proc-return))
	 (assign val (op compiled-procedure-entry) (reg proc))
	 (goto (reg val))
	 ,proc-return
	 (assign ,target (reg val))
	 (goto (label ,linkage))))))
   ;; Code for iterative process.
   ((and (eq? target 'val)
	 (eq? linkage 'return))
    (make-instruction-sequence
     '(proc continue) registers
     '((assign val (op compiled-procedure-entry) (reg proc))
       (goto (reg val)))))
   ((and (not (eq? target 'val))
	 (eq? linkage 'return))
    (error "Return linkage, target not val -- COMPILE" target))))

;; Return the object code for procedure call with target register TARGET and
;; linkage LINKAGE.
(define (compile-procedure-call target linkage env)
  (let* ((primitive-branch (make-label 'primitive-branch))
         (compiled-branch (make-label 'compiled-branch))
	 ;; Label inserted after `primitive-branch'.
         (after-call (make-label 'after-call))
	 ;; If linkage is `next' than jump to label `after-call'.
	 (compiled-linkage (if (eq? linkage 'next) after-call linkage)))
    (append-instruction-sequences
     (make-instruction-sequence
      '(proc) '()
      `((test (op primitive-procedure?) (reg proc))
	(branch (label ,primitive-branch))))
     (parallel-instruction-sequences
      (append-instruction-sequences
       compiled-branch
       (compile-proc-appl target compiled-linkage env))
      (append-instruction-sequences
       primitive-branch
       (end-with-linkage
	linkage
	(make-instruction-sequence
	 '(proc argl) (list target)
	 `((assign ,target
		   (op apply-primitive-procedure)
		   (reg proc)
		   (reg argl)))))))
     after-call)))

;; Compile procedure application expression EXP which targets register TARGET
;; and linkage LINKAGE.
(define (compile-application exp target linkage env)
  (let ((proc-code (compile (operator exp) 'proc 'next env))
        (operand-codes
         (map (lambda (operand) (compile operand 'val 'next env))
              (operands exp))))
    (preserving '(env continue)
		proc-code
		(preserving '(proc continue)
			    (construct-arglist operand-codes)
			    (compile-procedure-call target linkage env)))))

(define (compile-cond exp target linkage env)
  (compile-if (cond->if exp) target linkage env))

;; FIXME 2021-10-23: Solve SICP Exercise 5.44, currently our implementation
;; treats names of open-coded primitive procedures as reserved words.  If a
;; program tries to rebind such a name, our mechanism will try to open-code them
;; as primitive.

;; FIXME 2021-10-21: Solve SICP Exercise 5.38 part (d), find a way to neatly
;; handle all primitive arguments with arbitrary numbers of operands with
;; defining procedures for each one of them.
(define (compile-primitive exp target linkage env)
  (append-instruction-sequences
   (spread-arguments (operands exp) env)
   (make-instruction-sequence
    '(arg1 arg2) (list target)
    `((assign ,target (op ,(operator exp)) (reg arg1) (reg arg2))))))

;; Install procedures which are used in analyzing expressions.
(define (install-compile-package)
  (put 'compile 'quote compile-quoted)
  (put 'compile '+ compile-primitive)
  (put 'compile '- compile-primitive)
  (put 'compile '* compile-primitive)
  (put 'compile '= compile-primitive)
  (put 'compile 'set! compile-assignment)
  (put 'compile 'define compile-definition)
  (put 'compile 'lambda compile-lambda)
  (put 'compile 'if compile-if)
  (put 'compile 'begin compile-begin)
  (put 'compile 'cond compile-cond))

(define (compile exp target linkage env)
  "Compile expression EXP with target TARGET and linkage LINKAGE."
  (cond
   ((self-evaluating? exp)
    (compile-self-evaluating exp target linkage env))
   ((variable? exp)
    (compile-variable exp target linkage env))
   ((get 'compile (operator exp))
    => (lambda (proc) (proc exp target linkage env)))
   ((application? exp)
    (compile-application exp target linkage env))
   (else (error "Unknown expression type -- COMPILE" exp))))

(install-compile-package)

(define empty-compile-frame (make-compile-frame '()))

(define the-empty-compile-environment
  (make-compile-environment empty-compile-frame))

;;; compiler.scm ends here
