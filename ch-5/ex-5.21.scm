;; Controller instruction sequence for Count-leaves machine.

(use-modules (sicp ch-5 simulator))

(define (not-pair? x)
  (not (pair? x)))

(define count-leaves-operation-table
  (append `((not-pair? ,not-pair?))
	  operation-table))

;; By Oavis: <https://www.inchmeal.io/sicp/ch-5/ex-5.21>, after completing
;; iter-version, I was unable to backtrack and think about recursive one.
(define count-leaves-rec-machine
  (make-machine
   '(tree continue tmp count)
   count-leaves-operation-table
   '((assign continue (label done))

     loop
     (test (op null?) (reg tree))
     (branch (label base-case-1))
     (test (op not-pair?) (reg tree))
     (branch (label base-case-2))
     ;; Save current state and then start `car'-ing down the tree.
     (save continue)
     (assign continue (label after-car))
     (save tree)
     (assign tree (op car) (reg tree))
     (goto (label loop))

     after-car
     (restore tree)
     (assign tree (op cdr) (reg tree))
     (assign continue (label after-cdr))
     (save count)
     (goto (label loop))

     after-cdr
     (assign tmp (reg count))
     (restore count)
     (assign count (op +) (reg tmp) (reg count))
     (restore continue)
     (goto (reg continue))

     base-case-1
     (assign count (const 0))
     (goto (reg continue))

     base-case-2
     (assign count (const 1))
     (goto (reg continue))

     done)))

(define count-leaves-iter-machine
  (make-machine
   '(tree continue count)
   count-leaves-operation-table
   '((assign count (const 0))
     (assign continue (label done))

     loop
     (test (op null?) (reg tree))
     (branch (label base-case-1))
     (test (op not-pair?) (reg tree))
     (branch (label base-case-2))
     ;; Save current state and then start `car'-ing down the tree.
     (save continue)
     (assign continue (label after-car))
     (save tree)
     (assign tree (op car) (reg tree))
     (goto (label loop))

     after-car
     (restore tree)
     (restore continue)
     (assign tree (op cdr) (reg tree))
     (goto (label loop))

     ;; Empty list
     base-case-1
     (goto (reg continue))

     ;; Leaf
     base-case-2
     (assign count (op +) (reg count) (const 1))
     (goto (reg continue))

     done)))

(define (count-leaves tree)
  (set-register-contents! count-leaves-iter-machine 'tree tree)
  (simulator-start count-leaves-iter-machine)
  (get-register-contents count-leaves-iter-machine 'count))
