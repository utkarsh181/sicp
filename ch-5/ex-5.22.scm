(use-modules (sicp ch-5 simulator))

(define append-machine
  (make-machine
   '(x y continue tmp val)
   operation-table
   '((assign continue (label done))

     loop
     (test (op null?) (reg x))
     (branch (label base-case))
     (save continue)
     (save x)
     (assign continue (label after-append))
     (assign x (op cdr) (reg x))
     (goto (label loop))

     after-append
     (restore x)
     (restore continue)
     (assign tmp (op car) (reg x))
     (assign val (op cons) (reg tmp) (reg val))
     (goto (reg continue))

     base-case
     (assign val (reg y))
     (goto (reg continue))

     done)))

(define (simulate-append x y)
  (set-register-contents! append-machine 'x x)
  (set-register-contents! append-machine 'y y)
  (simulator-start append-machine)
  (get-register-contents append-machine 'val))

(define (last-pair x)
  (if (null? (cdr x))
      x
      (last-pair (cdr x))))

(define append!-operation-table
  (append `((set-cdr! ,set-cdr!))
	  operation-table))

(define append!-machine
  (make-machine
   '(x y tmp1 tmp2)
   append!-operation-table
   '((assign tmp1 (reg x))

     loop
     (assign tmp2 (op cdr) (reg tmp1))
     (test (op null?) (reg tmp2))
     (branch (label base-case))
     (assign tmp1 (op cdr) (reg tmp1))
     (goto (label loop))

     base-case
     (perform (op set-cdr!) (reg tmp1) (reg y)))))

(define (simulate-append! x y)
  (set-register-contents! append!-machine 'x x)
  (set-register-contents! append!-machine 'y y)
  (simulator-start append!-machine)
  (get-register-contents append!-machine 'x))
