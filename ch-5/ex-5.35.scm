;;; Scheme Code

(define (f x)
  (+ x (g (+ x 2))))

;;; Object Code

((val env)
 (val)
 ((assign val (op make-compiled-procedure) (label entry68) (reg env))
  (goto (label after-lambda69))

  entry68
  (assign env (op compiled-procedure-env) (reg proc))
  (assign env (op extend-environment) (const (x)) (reg argl) (reg env))
  (assign proc (op lookup-variable-value) (const +) (reg env))
  (save continue)
  (save proc)
  (save env)
  (assign proc (op lookup-variable-value) (const g) (reg env))
  (save proc)
  (assign proc (op lookup-variable-value) (const +) (reg env))
  (assign val (const 2))
  (assign argl (op list) (reg val))
  (assign val (op lookup-variable-value) (const x) (reg env))
  (assign argl (op cons) (reg val) (reg argl))
  (test (op primitive-procedure?) (reg proc))
  (branch (label primitive-branch70))

  compiled-branch71
  (assign continue (label after-call72))
  (assign val (op compiled-procedure-entry) (reg proc))
  (goto (reg val))

  primitive-branch70
  (assign val (op apply-primitive-procedure) (reg proc) (reg argl))

  after-call72
  (assign argl (op list) (reg val))
  (restore proc)
  (test (op primitive-procedure?) (reg proc))
  (branch (label primitive-branch73))

  compiled-branch74
  (assign continue (label after-call75))
  (assign val (op compiled-procedure-entry) (reg proc))
  (goto (reg val))

  primitive-branch73
  (assign val (op apply-primitive-procedure) (reg proc) (reg argl))

  after-call75
  (assign argl (op list) (reg val))
  (restore env)
  (assign val (op lookup-variable-value) (const x) (reg env))
  (assign argl (op cons) (reg val) (reg argl))
  (restore proc)
  (restore continue)
  (test (op primitive-procedure?) (reg proc))
  (branch (label primitive-branch76))

  compiled-branch77
  (assign val (op compiled-procedure-entry) (reg proc))
  (goto (reg val))

  primitive-branch76
  (assign val (op apply-primitive-procedure) (reg proc) (reg argl))
  (goto (reg continue))

  after-call78
  after-lambda69
  (perform (op define-variable!) (const f) (reg val) (reg env))
  (assign val (const ok))))
