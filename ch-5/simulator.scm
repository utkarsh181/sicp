;;; simulator.scm --- Register Machine Simulator

;;; Commentary:

;; This program implements Simulator for Register Machines, which can be used to
;; understand questions such as:
;;
;; 1. How the evaluation of an sub-expression manages to return a value to the
;; expression that uses this value?
;;
;; 2. How some recursive procedures generate iterative process whereas other
;; recursive procedure generate recursive process?
;;
;; This Simulator also provides extensive debugging facilities such as:
;;
;; 1. Instruction tracing with label annotation.  For example, to turn trace ON
;; for a machine called `foo', you will issue the following command:
;;
;; (foo 'toggle-trace)
;;
;; 2. Register tracing.  When a register is traced, assigning a value to the
;; register prints the name of the register, the old contents of the register,
;; and the new contents being assigned. For example, to turn trace ON for a
;; register `bar' in machine `foo', you will issue the following command:
;;
;; ((foo 'register-toggle-trace) 'bar)
;;
;; 3. Breakpoint mechanism.  With breakpoint you can stop simulator at your will
;; and then examine the state of the machine.  For example, to set breakpoint at
;; line number 2 of label `bar' in machine `foo', you will issue the following
;; command:
;;
;; (set-breakpoint foo 'bar 2)
;;
;; To learn more about breakpoint facilities, see SICP Exercise 5.19.
;;
;; Also see "../ch-4/beval.scm" for implementation of meta-circular evaluator,
;; "ex-5.2.scm" for demonstration of computation through Register Machines.

;;; Code:

(define-module (sicp ch-5 simulator)
  :use-module (ice-9 format)
  :export (operation-table

	   make-machine

	   get-register-contents
	   set-register-contents!

	   set-breakpoint
	   cancel-breakpoint
	   cancel-all-breakpoints
	   continue-machine

	   assemble
	   simulator-start))

;; Table of common operations for register machines.
(define operation-table
  `((+ ,+)
    (- ,-)
    (* ,*)
    (/ ,/)
    (= ,=)
    (> ,>)
    (< ,<)
    (abs ,abs)
    (cons ,cons)
    (list ,list)
    (car ,car)
    (cdr ,cdr)
    (null? ,null?)))

;;; Constructor

(define* (make-instruction text #:key label proc)
  (list label text proc))

(define (make-label-entry label-name insts)
  (cons label-name insts))

(define (make-stack)
  (let ((s '())
        (number-pushes 0)
        (max-depth 0)
        (current-depth 0))
    (define (push x)
      (set! s (cons x s))
      (set! number-pushes (1+ number-pushes))
      (set! current-depth (1+ current-depth))
      (set! max-depth (max current-depth max-depth)))

    (define (pop)
      (if (null? s)
          (error "Empty stack -- POP")
          (let ((top (car s)))
            (set! s (cdr s))
            (set! current-depth (1- current-depth))
            top)))

    (define (initialize)
      (set! s '())
      (set! number-pushes 0)
      (set! max-depth 0)
      (set! current-depth 0)
      'done)

    (define (print-statistics)
      (format #t "No. of pushes = ~d ~%" number-pushes)
      (format #t "Maximum depth = ~d ~%" max-depth))

    (define (dispatch message)
      (cond
       ((eq? message 'push) push)
       ((eq? message 'pop) (pop))
       ((eq? message 'initialize) (initialize))
       ((eq? message 'print-statistics)
        (print-statistics))
       (else (error "Unknown request -- STACK" message))))
    dispatch))

;; Syntax for register expression: (reg <name>)

;; Return a procedure that models register named as NAME.
(define (make-register name)
  (let ((contents '*unassigned*)
	(trace #f))			; register trace flag
    (define (toggle-trace)
      (if trace
	  (set! trace #f)
	  (set! trace #t))
      (format #t "Register ~a trace: ~a~%" name trace))

    (define (dispatch message)
      (cond
       ((eq? message 'get) contents)
       ((eq? message 'set)
        (lambda (value)
	  (when trace
	    (format #t "reg trace: ~a <- ~a (was ~a)~%" name value contents))
	  (set! contents value)))
       ((eq? message 'toggle-trace) (toggle-trace))
       (else (error "Unknown request -- REGISTER" message))))
    dispatch))

(define (make-primitive-exp exp machine labels)
  (cond
   ((constant-exp? exp)
    (let ((c (constant-exp-value exp)))
      (lambda () c)))
   ((label-exp? exp)
    (let ((insts (lookup-label labels (label-exp-label exp))))
      (lambda () insts)))
   ((register-exp? exp)
    (let ((r (get-register machine (register-exp-reg exp))))
      (lambda () (get-contents r))))
   (else
    (error "Unknown expression type -- ASSEMBLE" exp))))

;; Lookup primitive operation SYMBOL in alist OPERATIONS.
(define (lookup-prim machine symbol operations)
  (cond
   ((assoc symbol operations) => cadr)
   ((eq? symbol 'stack-initialize)
    (lambda () ((machine 'stack) 'initialize)))
   ((eq? symbol 'stack-print-statistics)
    (lambda () ((machine 'stack) 'print-statistics)))
   (else (error "Unknown operation -- ASSEMBLE" symbol))))

;; Syntax for operation: (op <name> <args-1> ... <arg-n>)

(define (make-operation-exp exp machine labels operations)
  (let ((op (lookup-prim machine (operation-exp-op exp) operations))
        (aprocs (map (lambda (e) (make-primitive-exp e machine labels))
		     (operation-exp-operands exp))))
    (lambda ()
      (apply op (map (lambda (p) (p)) aprocs)))))

;; Syntax for assignment expression: (assign <var> <value>)

(define (make-assign inst machine labels operations pc)
  (let* ((target (get-register machine (assign-reg-name inst)))
         (value-exp (assign-value-exp inst))
	 (value-proc
          (if (operation-exp? value-exp)
              (make-operation-exp
               value-exp machine labels operations)
              (make-primitive-exp
               (car value-exp) machine labels))))
    ;; Execution procedure for assign
    (lambda ()
      (set-contents! target (value-proc))
      (advance-pc pc))))

;; Syntax for branch: (branch (label <exp>)) OR (branch (reg <exp>))

(define (make-branch inst machine labels flag pc)
  (let ((dest (branch-dest inst)))
    (cond
     ((register-exp? dest)
      (lambda ()
	(let* ((reg (register-exp-reg dest))
	       (label (get-register-contents machine reg)))
	((make-branch `(branch (label ,label)) machine labels flag pc)))))
     ((label-exp? dest)
      (lambda ()
	;; If flag is true, then branch to `dest'.
        (if (get-contents flag)
            (set-contents! pc
			   (lookup-label labels (label-exp-label dest)))
            (advance-pc pc))))
     (else (error "Bad BRANCH instruction -- ASSEMBLE" inst)))))

;; Syntax for test: (test <condition>)

(define (make-test inst machine labels operations flag pc)
  (let ((condition (test-condition inst)))
    (if (operation-exp? condition)
        (let ((condition-proc
               (make-operation-exp
                condition machine labels operations)))
          (lambda ()
	    ;; At simulation time, assign result to FLAG register.
            (set-contents! flag (condition-proc))
            (advance-pc pc)))
        (error "Bad TEST instruction -- ASSEMBLE" inst))))

;; Syntax for goto: (goto (label <exp>)) OR (goto (reg <exp>))

(define (make-goto inst machine labels pc)
  (let ((dest (goto-dest inst)))
    (cond
     ((label-exp? dest)
      (let ((insts (lookup-label labels (label-exp-label dest))))
        (lambda () (set-contents! pc insts))))
     ((register-exp? dest)
      (let ((reg (get-register machine (register-exp-reg dest))))
        (lambda ()
          (set-contents! pc (get-contents reg)))))
     (else (error "Bad GOTO instruction -- ASSEMBLE" inst)))))

(define (make-save inst machine stack pc)
  (let ((reg (get-register machine (stack-inst-reg-name inst))))
    (lambda ()
      (push stack (get-contents reg))
      (advance-pc pc))))

(define (make-restore inst machine stack pc)
  (let ((reg (get-register machine (stack-inst-reg-name inst))))
    (lambda ()
      (set-contents! reg (pop stack))
      (advance-pc pc))))

(define (make-perform inst machine labels operations pc)
  (let ((action (perform-action inst)))
    (if (operation-exp? action)
        (let ((action-proc
               (make-operation-exp
                action machine labels operations)))
          (lambda ()
            (action-proc)
            (advance-pc pc)))
        (error "Bad PERFORM instruction -- ASSEMBLE" inst))))

;; TODO 2021-10-05: Implement data-path generator as specified in Exercise 5.12
;; and 5.13.  One possible method to do so would be to collect information in
;; assembling time, but doing so would significantly increase number of formal
;; arguments of `make-*' procedures (or constructors).

;; Make an execution procdure from instruction INST for machine MACHINE at
;; program counter PC with labels and operations as LABELS and OPERATIONS
;; respectively.
(define (make-execution-procedure inst labels machine pc flag stack ops)
  (cond
   ((eq? (car inst) 'assign)
    (make-assign inst machine labels ops pc))
   ((eq? (car inst) 'test)
    (make-test inst machine labels ops flag pc))
   ((eq? (car inst) 'branch)
    (make-branch inst machine labels flag pc))
   ((eq? (car inst) 'goto)
    (make-goto inst machine labels pc))
   ((eq? (car inst) 'save)
    (make-save inst machine stack pc))
   ((eq? (car inst) 'restore)
    (make-restore inst machine stack pc))
   ((eq? (car inst) 'perform)
    (make-perform inst machine labels ops pc))
   (else (error "Unknown instruction type -- ASSEMBLE" inst))))

(define (make-breakpoint)
  (let ((breakpoint-table (make-hash-table)))
    (define (dispatch message)
      (cond
       ((eq? message 'set)
	(lambda (breakpoint) (hash-set! breakpoint-table breakpoint #t)))
       ((eq? message 'get)
	(lambda (breakpoint) (hash-ref breakpoint-table breakpoint)))
       ((eq? message 'cancel)
	(lambda (breakpoint) (hash-set! breakpoint-table breakpoint #f)))
       ((eq? message 'cancel-all)
	(lambda () (hash-clear! breakpoint-table)))
       (else (error "Unknown request -- BREAKPOINT" message))))
    dispatch))

(define (make-new-machine)
  (let* ((pc (make-register 'pc))	; program counter
         (flag (make-register 'flag))
         (stack (make-stack))
	 (inst-count 0)			; instruction counter
	 (block-count 0)		; no. of instruction b/w two labels
	 (trace #f)			; instruction trace flag
	 (current-label '())
         (the-instruction-sequence '())
	 (the-ops '())
         (register-table `((pc ,pc) (flag ,flag)))
	 (breakpoint (make-breakpoint)))
    (define (allocate-register name)
      (if (assoc name register-table)
          (error "Multiply defined register: " name)
          (set! register-table
                (cons (list name (make-register name)) register-table)))
      'register-allocated)

    (define (lookup-register name)
      (let ((val (assoc name register-table)))
        (if val
            (cadr val)
            (error "Unknown register:" name))))

    (define (get-breakpoint label line-number)
      ((breakpoint 'get) (list label line-number)))

    ;; Execute all instructions.
    (define (execute)
      (let ((insts (get-contents pc)))
        (if (null? insts)
            'done
	    (let* ((inst (car insts))
		   (inst-label (instruction-label inst))
		   (inst-text (instruction-text inst))
		   (inst-proc (instruction-execution-proc inst)))

	      (set! inst-count (1+ inst-count))

	      (if (not inst-label)
		  (set! block-count (1+ block-count))
		  (begin
		    (set! current-label inst-label)
		    (set! block-count 1)))

	      ;; Display trace
	      (when trace
		(when inst-label
		  (format #t "label: ~a~%" inst-label))
		(format #t "~a) trace: ~a~%" block-count inst-text))

	      ;; If there are NO breakpoints then `apply' the execution
	      ;; procedure.
	      (if (get-breakpoint current-label block-count)
		  (begin
		    (format #t
			    "breakpoint: (label: ~a line-number: ~a)~%"
			    current-label
			    block-count)
		    ;; NOTE 2021-10-06: Do we have to restore program counter?
		    (set-contents! pc insts))
		  (begin
		    (inst-proc)
		    (execute)))))))

    (define (print-and-reset-inst-count)
      (format #t "No. of instruction = ~d~%" inst-count)
      (set! inst-count 0))

    ;; Toggle tracing of instructions execution.
    (define (toggle-trace)
      (if trace
	  (set! trace #f)
	  (set! trace #t))
      (format #t "Instruction trace: ~a~%" trace))

    ;; Toggle tracing for registor NAME
    (define (register-toggle-trace name)
      (let ((reg (assoc name register-table)))
	(if reg
            ((cadr reg) 'toggle-trace)
            (error "Unknown register: " name))))

    (define (dispatch message)
      (cond
       ((eq? message 'start)
        (set-contents! pc the-instruction-sequence)
	(set! block-count 0)
        (execute))
       ((eq? message 'continue)
	(execute))
       ((eq? message 'install-instruction-sequence)
        (lambda (seq) (set! the-instruction-sequence seq)))
       ((eq? message 'allocate-register) allocate-register)
       ((eq? message 'get-register) lookup-register)
       ((eq? message 'install-operations)
        (lambda (ops) (set! the-ops (append the-ops ops))))
       ((eq? message 'stack) stack)
       ((eq? message 'operations) the-ops)
       ((eq? message 'instruction-count) (print-and-reset-inst-count))
       ((eq? message 'toggle-trace) (toggle-trace))
       ((eq? message 'register-toggle-trace) register-toggle-trace)
       ((eq? message 'breakpoint) breakpoint)
       (else (error "Unknown request -- MACHINE" message))))
    dispatch))

(define (make-machine register-names ops controller-text)
  "Return a machine model with list of registers REGISTER-NAMES, operations OPS
and controller sequence CONTROLLER-TEXT."
  (let ((machine (make-new-machine)))
    (for-each (lambda (register-name)
                ((machine 'allocate-register) register-name))
              register-names)
    ((machine 'install-operations) ops)
    ((machine 'install-instruction-sequence)
     (assemble controller-text machine))
    machine))

;;; Selector

(define (register-exp-reg exp) (cadr exp))

(define (constant-exp-value exp) (cadr exp))

(define (label-exp-label exp) (cadr exp))

(define (operation-exp-op operation-exp)
  (cadr (car operation-exp)))

(define (operation-exp-operands operation-exp)
  (cdr operation-exp))

;; Return name of register in assignment expression.
(define (assign-reg-name assign-instruction)
  (cadr assign-instruction))

;; Return value expression in assignment expression.
(define (assign-value-exp assign-instruction)
  (cddr assign-instruction))

(define (branch-dest branch-instruction)
  (cadr branch-instruction))

(define (test-condition test-instruction)
  (cdr test-instruction))

(define (goto-dest goto-instruction)
  (cadr goto-instruction))

(define (stack-inst-reg-name stack-instruction)
  (cadr stack-instruction))

(define (perform-action inst) (cdr inst))

(define (instruction-label inst)
  (car inst))

(define (instruction-text inst)
  (cadr inst))

(define (instruction-execution-proc inst)
  (caddr inst))

(define (get-contents register)
  (register 'get))

(define (get-register-contents machine register-name)
  "Return contents of register REGISTER-NAME in machine MACHINE."
  (get-contents (get-register machine register-name)))

(define (get-register machine reg-name)
  ((machine 'get-register) reg-name))

;;; Predicates

;; Is expression EXP tagged TAG?
(define (tagged-list? exp tag)
  (if (pair? exp)
      (eq? (car exp) tag)
      #f))

(define (operation-exp? exp)
  (and (pair? exp) (tagged-list? (car exp) 'op)))

(define (register-exp? exp) (tagged-list? exp 'reg))

(define (constant-exp? exp) (tagged-list? exp 'const))

(define (label-exp? exp) (tagged-list? exp 'label))

;;; Mutators

(define (set-instruction-label! inst label)
  (set-car! inst label))

(define (set-instruction-execution-proc! inst proc)
  (set-car! (cddr inst) proc))

(define (pop stack)
  (stack 'pop))

(define (push stack value)
  ((stack 'push) value))

(define (set-contents! register value)
  ((register 'set) value))

(define (set-register-contents! machine register-name value)
  "Set register REGISTER-VALUE to value VALUE in machine MACHINE."
  (set-contents! (get-register machine register-name) value)
  'done)

;; Advance program counter PC to next set of instructions.
(define (advance-pc pc)
  (set-contents! pc (cdr (get-contents pc))))

;; Update instruction INSTS with labels LABELS for machine MACHINE.
(define (update-insts! insts labels machine)
  (let ((pc (get-register machine 'pc))
        (flag (get-register machine 'flag))
        (stack (machine 'stack))
        (ops (machine 'operations)))
    (for-each
     (lambda (inst)
       (set-instruction-execution-proc!
        inst
        (make-execution-procedure
         (instruction-text inst) labels machine pc flag stack ops)))
     insts)))

;;; General Procedures

(define (lookup-label labels label-name)
  (let ((val (assoc label-name labels)))
    (if val
        (cdr val)
        (error "Undefined label -- ASSEMBLE" label-name))))

(define (set-breakpoint machine label line-number)
  "Set breakpoint in REGISTER MACHINE at LINE-NUMBER lines away from label LABEL."
  (((machine 'breakpoint) 'set) (list label line-number)))

(define (cancel-breakpoint machine label line-number)
  "Cancel breakpoint in REGISTER MACHINE at LINE-NUMBER lines away from label LABEL."
  (((machine 'breakpoint) 'cancel) (list label line-number)))

(define (cancel-all-breakpoints machine)
  "Cancel all breakpoints for register machine MACHINE."
  (((machine 'breakpoint) 'cancel-all)))

;; Return a list of instructions and a `alist' associating label to instructions
;; from controller text TEXT.
(define (extract-labels text)
  ;; Add label LABEL to first instruction of instructions INSTRUCTIONS.
  (define (add-label insts label)
    (if (null? insts)
	'()
	(let ((inst-text (instruction-text (car insts))))
	  (cons (make-instruction inst-text #:label label)
		(cdr insts)))))

  (if (null? text)
      (cons '() '())
      (let* ((result (extract-labels (cdr text)))
	     (insts (car result))
	     (labels (cdr result))
	     (next-inst (car text)))
        (if (symbol? next-inst)
	    (if (assq next-inst labels)	;REVIEW 2021-10-04: Is `eq' sufficient?
		(error "Ambiguous label -- EXTRACT-LABELS" next-inst)
		(let ((labelled-inst (add-label insts next-inst)))
		  (cons labelled-inst
			(cons (make-label-entry next-inst labelled-inst) labels))))
	    (cons (cons (make-instruction next-inst) insts) labels)))))

;; Assemble/Transform controller sequence CONTROLLER-TEXT for MACHINE into
;; corresponding machine instruction.
(define (assemble controller-text machine)
  (let* ((result (extract-labels controller-text))
	 (insts (car result))
	 (labels (cdr result)))
    (update-insts! insts labels machine)
    insts))

(define (simulator-start machine)
  "Start simulating register machine MACHINE."
  (machine 'start))

(define (continue-machine machine)
  "Continue simulating register machine MACHINE.

Simulation of register machine usually can be manipulated with
`set-breakpoint'."
  (machine 'continue))

;;; simulator.scm ends here
