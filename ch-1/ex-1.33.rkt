#lang racket

(provide accumulate)

(require "ex-1.3.rkt")
(require "ex-1.22.rkt")

;;; Iterative Process

(define (accumulate combiner null-value predicate term a next b)
  (define (check a)
    (if (not (predicate a))
        null-value
        (term a)))
  (define (iter a result)
    (if (> a b)
        result
        (iter (next a)
              (combiner (check a) result))))
  (iter a null-value))

;;; Recursive Process

(define (accumulate-rec combiner null-value predicate term a next b)
  (define (check a)
    (if (not (predicate a))
        null-value
        (term a)))
  (if (> a b)
      null-value
      (combiner (check a)
                (accumulate-rec combiner null-value predicate
                                term (next a) next b))))

;;; Procedures

;; Return sum of prime numbers in interval A and B.
(define (sum-of-square-prime a b)
  (accumulate + 0 prime? square a add1 b))

(define (identity a) a)

(define (gcd a b)
  (if (= b 0)
      a
      (gcd b (remainder a b))))

;; Return product of all the positive integers less than N that are relatively
;; prime to N.
(define (product-of-relative-prime n)
  (define (relative-prime? x)
    (= (gcd x n) 1))
  (accumulate * 1 relative-prime? identity 1 add1 n))
