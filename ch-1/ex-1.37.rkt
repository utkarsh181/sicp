#lang racket

(provide cont-frac)

;;; Recursive process

(define (cont-frac-rec n d k)
  (define (cont-frac-rec-1 i)
    (if (= i k)
        0
        (/ (n i) (+ (d i) (cont-frac-rec-1 (add1 i))))))
  (cont-frac-rec-1 1))

;;; Iterative process

(define (cont-frac n d k)
  (define (cont-frac-iter n d k result)
    (if (zero? k)
        result
        (cont-frac-iter n d (sub1 k) (/ (n k) (+ (d k) result)))))
  (cont-frac-iter n d k 0))

;;; Procedures

;; NOTE 2021-08-01: `cont-frac-rec' and `cont-frac' requires N equal to 13 and
;; 12 respectively in order to approximate golden ratio to 4 decimal places.

(define (golden-ratio n)
  (expt (cont-frac (lambda (i) 1.0)
                   (lambda (i) 1.0)
                   n)
        -1))
