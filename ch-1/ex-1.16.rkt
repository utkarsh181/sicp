#lang racket

(require "ex-1.3.rkt")

(provide fast-expt)

;; TODO 2021-07-28: What about negative exponents?

;;; Recursive process

(define (fast-expt-rec b n)
  (cond [(= n 0)
         1]
        [(even? n)
         (square (fast-expt-rec b (/ n 2)))]
        [else
         (* b (fast-expt-rec b (- n 1)))]))

;;; Iterative process

(define (fast-expt b n)
  (define (fast-expt-iter b a n)
  (cond
    [(= n 0)
     a]
    [(even? n)
     (fast-expt-iter (square b) a (/ n 2))] ; Isn't this cool?
    [else
     (fast-expt-iter b (* a b) (sub1 n))]))
  (fast-expt-iter b 1 n))
