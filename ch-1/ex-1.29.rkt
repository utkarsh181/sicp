#lang racket

(require "ex-1.8.rkt")

;; Return sum of TERM's from A to B while using NEXT as incrementing procedure.
(define (sum term a next b)
  (if (> a b)
      0
      (+ (term a)
         (sum term (next a) next b))))

;; Integrate function F from A to B from some even iterger N (increasing N
;; increases the accuracy of the approximation) using Simpson's Rule.
(define (integral f a b n)
  (define h (/ (- b a) n))
  (define (y k)
    (* (cond
         [(zero? k) 1]
         [(even? k) 4]
         [else 2])
       (f (+ a (* k h)))))

  (* (/ h 3)
     (sum y 0 add1 n)))
