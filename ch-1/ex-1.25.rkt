#lang racket

(require "ex-1.3.rkt")
(require "ex-1.16.rkt")

(define (expmod base exp m)
  (remainder (fast-expt base exp) m))

(define (fermat-test n)
  (define (try-it a)
    (= (expmod a n n) a))

  (try-it (+ 1 (random (- n 1)))))

(define (prime? n)
  (define (fast-prime? n times)
    (cond [(= times 0) true]
          [(fermat-test n)
           (fast-prime? n (- times 1))]
          [else false]))

  ;; REVIEW 2021-07-29: How can we test if log(n) tests are sufficient?
  (fast-prime? n (floor (log n))))

(define (timed-prime-test n)
  (define (report-prime elapsed-time)
    (display " *** ")
    (display elapsed-time))

  (define (start-prime-test n start-time)
    (when (prime? n)
      (report-prime (- (current-inexact-milliseconds)
                       start-time))))

  (newline)
  (display n)
  (start-prime-test n (current-inexact-milliseconds)))

(define (search-for-primes lower upper)
  (cond
    [(>= lower upper)
     (newline)]
    [(even? lower)
     (search-for-primes (add1 lower) upper)]
    [else
     (timed-prime-test lower)
     (search-for-primes (+ lower 2) upper)]))
