#lang racket

(provide repeated)

(require "ex-1.42.rkt")

(define (identity x) x)

;; NOTE 2021-08-01: Earlier I was using a iterative version for this procedure.
;; Recursive process adapted from Eli's soulution:
;; <https://eli.thegreenplace.net/2007/07/19/sicp-section-134>

;; Return the procedure F repeated N times.
(define (repeated f n)
  (if (= n 0)
      identity
      (compose f (repeated f (sub1 n)))))
