#lang racket

;; TODO 2021-07-28: What about negative numbers?

(define (double a) (+ a a))

(define (halve a) (/ a 2))

(define (multiply a b)
  (cond [(= b 0)
         0]
        [(even? b)
         (double (multiply a (halve b)))]
        [else
         (+ a (multiply a (sub1 b)))]))
