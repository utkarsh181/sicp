#lang racket

(provide prime?)

(require "ex-1.3.rkt")

(define (divides? a b)
  (= (remainder b a) 0))

(define (smallest-divisor n)
  (define (find-divisor n test-divisor)
    (cond
      [(> (square test-divisor) n)
       n]
      [(divides? test-divisor n)
       test-divisor]
      [else
       (find-divisor n (add1 test-divisor))]))
  (find-divisor n 2))

(define (prime? n)
  (= n (smallest-divisor n)))

(define (timed-prime-test n)
  (define (report-prime elapsed-time)
    (display " *** ")
    (display elapsed-time))

  (define (start-prime-test n start-time)
    (when (prime? n)
      (report-prime (- (current-inexact-milliseconds)
                       start-time))))
  (newline)
  (display n)
  (start-prime-test n (current-inexact-milliseconds)))

(define (search-for-primes lower upper)
  (cond
    [(>= lower upper)
     (newline)]
    [(even? lower)
     (search-for-primes (add1 lower) upper)]
    [else
     (timed-prime-test lower)
     (search-for-primes (+ lower 2) upper)]))
