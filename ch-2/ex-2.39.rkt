#lang racket/base

(define A '(1 2 3 4 5))

(define (reverse sequence)
  (foldr
   (lambda (x y) (append y (list x))) null sequence))

;; (define (reverse sequence)
;;   (foldl cons null sequence))
