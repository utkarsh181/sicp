#lang racket

(define (for-each f item)
  (unless (null? item)
    (f (car item))
    (for-each f (cdr item))))
