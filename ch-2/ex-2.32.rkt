#lang racket/base

(require racket/trace)

;; Return all subsets of S which is a list of distinct element.
(define (subsets s)
  (if (null? s)
      (list null)
      (let ((rest (subsets (cdr s))))
        (append rest
                (map (lambda (x) (cons (car s) x)) ; Magic!
                     rest)))))
