#+title: SICP Exercise 2.19
#+author: Utkarsh Singh

* Order of Coins

In this case order of coins will *not* affect the result of the
procedure =cc=.  In [[file:~/Documents/progs/racket/sicp/ch-1/ex-1.14.org][Exercise 1.14]] we were using =first-denomination=
which specified denomination based on =kind-of-coins=, a number which
keeps check of denomination of coins left with us.  But now we are
using =india-coin=, a /list structure/ and =except-first-denomination=
which recursively discards the coins exhausted in the change-making
process.
