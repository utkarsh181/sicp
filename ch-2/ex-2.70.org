#+title: SICP Exercise 2.70
#+author: Utkarsh Singh

* Introduction

#+begin_src racket
(define rock-symbols '((A 2)
                       (NA 16)
                       (BOOM 1)
                       (SHA 3)
                       (GET 2)
                       (YIP 9)
                       (JOB 2)
                       (WAH 1)))

(define rock-song
  '(GET A JOB
        SHA NA NA NA NA NA NA NA NA
        GET A JOB
        SHA NA NA NA NA NA NA NA NA
        WAH YIP YIP YIP YIP YIP YIP YIP YIP YIP
        SHA BOOM))

(length (encode rock-song (generate-huffman-tree rock-symbols)))
#+end_src

* Explanation

According to Huffman Encoding algorithm, the rock song will require
*84* bits.  For fixed length, smallest no. of bits per symbol will be
=(log 2 (length rock-symbols))=, that is 3.  This means that this song
we will require *108* bits in fixed lenght encoding.
