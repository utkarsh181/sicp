#lang racket/base

(provide apply-generic)

(require "tags.rkt")

;; Apply generic operation OP on ARGS
(define (apply-generic op . args)
  ;; TODO 2021-08-21: Test it
  (define (no-method)
    (raise-arguments-error 'apply-generic
                           "no method for the type"
                           "op" op
                           "types" (map type-tag args)))

  ;; Can ARGS we coerced into type TYPE?
  (define (coerce? type)
    (andmap (lambda (x)
              (or (equal? type (type-tag x))
                  (get 'coercion (list (type-tag x) type))))
            args))

  ;; Corece all ARGS to type TYPE
  (define (coerce-all type)
    (map (lambda (x)
           (if (equal? type (type-tag x))
               x
               ((get 'coercion (list (type-tag x) type)) x)))
         args))

  (define (iter tags)
    (let ([proc (get op tags)])
      (if proc
          (apply proc (map contents args))
          ;; REVIEW 2021-08-21: Are all base cases covered?
          (cond
            [(null? tags) (no-method)]
            [(coerce? (car tags))
             (apply apply-generic op (coerce-all (car tags)))]
            [else
             (iter (cdr tags))]))))
  (iter (map type-tag args)))
