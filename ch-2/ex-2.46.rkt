#lang racket

(provide make-vect
         xcor-vect
         ycor-vect
         add-vect
         sub-vect
         scale-vect)

;;; Constructors

(define (make-vect x y)
  (list x y))

;;; Selectors

(define (xcor-vect v)
  (car v))

(define (ycor-vect v)
  (cadr v))

;;; General Procedures

(define (add-vect v1 v2)
  (make-vect (+ (xcor-vect v1)
               (xcor-vect v1))
            (+ (ycor-vect v1)
               (ycor-vect v1))))

(define (sub-vect v1 v2)
  (make-vect (- (xcor-vect v1)
               (xcor-vect v1))
            (- (ycor-vect v1)
               (ycor-vect v1))))

(define (scale-vect s v)
  (make-vect (* s
               (xcor-vect v))
            (* s
               (ycor-vect v))))
