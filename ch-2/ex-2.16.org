#+title: SICP Exercise 2.16
#+author: Utkarsh Singh

# TODO 2021-08-05: Add more mathematical evidence

As shown in [[file:ex-2.14.org][Exercise 2.14]], even though both equation are
arithmetically equivalent, interval operation does *not* follow
divisible rule and are not distributive in nature.
