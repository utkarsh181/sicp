#lang racket/base

(provide apply-generic)

(require "ex-2.78.rkt")
(require "table.rkt")

(require racket/trace)

;; Apply generic operation OP on ARGS
(define (apply-generic op . args)
  (define (no-method type-tags)
    (raise-arguments-error 'apply-generic
                           "no method for the type"
                           "op" op
                           "types" type-tags))
  (let* ([type-tags (map type-tag args)]
         [proc (get op type-tags)])
    (if proc
        ;; FIXME 2021-08-26: What a breakdown of abstraction!
        (if (memq op '(equ? =zero? raise reduce))
            (apply proc (map contents args))
            (drop (apply proc (map contents args))))
        (if (= (length args) 2)
            (let* ([type1 (car type-tags)]
                   [type2 (cadr type-tags)]
                   [a1 (car args)]
                   [a2 (cadr args)]
                   [r1 (get 'raise (list type1))]
                   [r2 (get 'raise (list type2))])
              (cond
                [(and (not r1) (not r2))
                 (no-method type-tags)]
                [(and r1 (not r2))
                 (apply-generic op (r1 (contents a1)) a2)]
                [(and (not r1) r2)
                 (apply-generic op a1 (r2 (contents a2)))]
                [else
                 (apply-generic op (r1 (contents a1)) (r2 (contents a2)))]))
            (no-method type-tags)))))

(define (equ? x y)
  (apply-generic 'equ? x y))

;; Raise X in type hierarchy.
(define (raise x)
  (apply-generic 'raise x))

;; Drop X to simplest possible type.
(define (drop x)
  (let ([project-f (get 'project (list (type-tag x)))])
    (if (not project-f)
        x
        (let ([dropped (project-f (contents x))])
          ;; Drop X if it's `raise' is equivalent to current value.
          (if (not (equ? x (raise dropped)))
              x
              (drop dropped))))))

(trace apply-generic)
(trace drop)
