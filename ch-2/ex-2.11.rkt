#lang racket

(require "ex-2.7.rkt")

(define (not-negative? x)
  (not (negative? x)))

(define (mul-interval x y)
  (let ((lx (lower-bound x))
        (ux (upper-bound x))
        (ly (lower-bound y))
        (uy (upper-bound y)))
    (cond
      ;; ++x
      [(and (not-negative? lx) (not-negative? ux))
       (cond
         [(and (not-negative? ly) (not-negative? uy)) ; ++y
          (make-interval (* lx ly) (* ux uy))]
         [(and (negative? ly) (not-negative? uy)) ; -+y
          (make-interval (* ux ly) (* ux uy))]
         [(and (negative? ly) (negative? uy)) ; --y
          (make-interval (* ux ly) (* lx uy))])]
      ;; -+x
      [(and (negative? lx) (not-negative? ux))
       (cond
         [(and (not-negative? ly) (not-negative? uy)) ; ++y
          (make-interval (* lx uy) (* ux uy))]
         [(and (negative? ly) (not-negative? uy)) ; -+y
          (let ((p1 (* lx ly))
                (p2 (* lx uy))
                (p3 (* ux ly))
                (p4 (* ux uy)))
            (make-interval (min p1 p2 p3 p4)
                           (max p1 p2 p3 p4)))]
         [(and (negative? ly) (negative? uy)) ; --y
          (make-interval (* ux ly) (* lx ly))])]
      ;; --x
      [(and (negative? lx) (negative? ux))
       (cond
         [(and (not-negative? ly) (not-negative? uy)) ; ++y
          (make-interval (* lx uy) (* ux ly))]
         [(and (negative? ly) (not-negative? uy)) ; -+y
          (make-interval (* lx uy) (* lx ly))]
         [(and (negative? ly) (negative? uy)) ; --y
          (make-interval (* ux uy) (* lx ly))])])))
