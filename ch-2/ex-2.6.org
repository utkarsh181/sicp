#+title: SICP Exercise 2.6
#+author: Utkarsh Singh

* Substitution Model

#+begin_src lisp-data
(add-1 zero)
(add-1 (lambda (f) (lambda (x) x)))
(lambda (f) (lambda (x) (f (((lambda (f) (lambda (x) x)) f) x))))
(lambda (f) (lambda (x) (f x)))
#+end_src

* Result

The following source block present usage of our =add= procedure with
the help of Racket's primitive =add1= procedure.

#+begin_src lisp-data
((one add1) 1)
2
(((add zero one) add1) 1)
2

((two add1) 1)
3
(((add zero two) add1) 1)
3
#+end_src
