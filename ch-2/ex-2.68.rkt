#lang racket/base

(provide encode)

(require "ex-2.67.rkt")

(define (encode-symbol message tree)
  (define (iter message result tree)
    (if (leaf? tree)
        (reverse result)
        (cond
          [(memq message (symbols (left-branch tree)))
           (iter message (cons 0 result) (left-branch tree))]
          [(memq message (symbols (right-branch tree)))
           (iter message (cons 1 result) (right-branch tree))]
          [else
           (raise-arguments-error 'encode-symbol
                                  "invalid message"
                                  "message" message)])))
  (iter message '() tree))

(define (encode message tree)
  (if (null? message)
      '()
      (append (encode-symbol (car message) tree)
              (encode (cdr message) tree))))
