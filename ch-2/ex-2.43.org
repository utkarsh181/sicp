#+title: SICP Exercise 2.44
#+author: Utkarsh Singh

* Problem

#+begin_src racket
(append-map
 (lambda (new-row)
   (map (lambda (rest-of-queens)
          (adjoin-position
           new-row k rest-of-queens))
        (queen-cols (- k 1))))
 (inclusive-range 1 board-size))
#+end_src

By interchanging the order of =(queen-cols (- k 1))= and
=(inclusive-range 1 board-size)=, we have increased the time
complexity our algorithm from O(n!) to O(n^n).  This mean, previously
for board-size N we calculated queens position for 1, 2, 3 ... N-1
board-sizes, but now for similar operation we are calculating 1, 2, 3
... N-1 positions N time each.
