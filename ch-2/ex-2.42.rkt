#lang racket

(define (make-position row col)
  (list row col))

(define (position-row pos)
  (car pos))

(define (position-col pos)
  (cadr pos))

(define empty-board null)

(define (adjoin-position row col rest-of-queens)
  (append (list (make-position row col))
          rest-of-queens))

;; Check if queen Q1 and Q2 will attack each other.
(define (attack? q1 q2)
  (let ((x1 (position-row q1))
        (y1 (position-col q1))
        (x2 (position-row q2))
        (y2 (position-col q2)))
    (or (= x1 x2)                         ; row attack
        (= y1 y2)                         ; column attack
        ;; NOTE 2021-08-09: By Eli:
        ;; <https://eli.thegreenplace.net/2007/08/22/sicp-sections-223>
        (= (abs (- x1 x2))                ; diagonal attack
           (abs (- y1 y2))))))

;; Check if queen at 0th index (car) is safe from all other queens (cdr) in
;; POSITIONS.
(define (safe? positions)
  (define (iter result new-pos positions)
    (cond
      [(or (false? result)
           (null? positions))
       result]
      [else
       (iter (not (attack? new-pos (car positions)))
             new-pos
             (cdr positions))]))
  (iter #t (car positions) (cdr positions)))

;; Return postions of safe queens in chess board of size BOARD-SIZE.
(define (queens board-size)
  (define (queen-cols k)
    (if (= k 0)
        (list empty-board)
        (filter
         safe?
         (append-map
          (lambda (rest-of-queens)
            (map (lambda (new-row)
                   (adjoin-position new-row k rest-of-queens))
                 (inclusive-range 1 board-size)))
          (queen-cols (sub1 k))))))
  (queen-cols board-size))
