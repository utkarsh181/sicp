#lang racket

(define (last-pair l)
  (let ((tmp (cdr l)))
    (if (null? tmp)
        (list (car l))
        (last-pair (cdr l)))))
