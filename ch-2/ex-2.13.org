#+title: SICP Exercise 2.13
#+author: Utkarsh Singh

* Proof

Let Z be a product of inverval (X + dx) and (Y + dy) where dx and dy
are respective tolerance.

#+begin_math
Z + dz = (X + dx) * (Y + dy)
Z + dz = XY + Xdy + Ydx + dxdy
dz = Xdy + Ydx [Z = XY; dxdy << 0]

Dividing equation by XY on both sides.

dz / XY = (Xdy / XY) + (Ydx / XY)
dz / Z = dy / Y + dx / X
%-tolerance of Z = (%-tolerance of X) + (%-tolerance of Y)
#+end_math
