#lang racket

(provide sign-change-detector
         zero-crossing)

(require "ex-3.50.rkt")
(require "ex-3.73.rkt")

(define (sign-change-detector current-sign last-sign)
  (cond
    [(and (positive? last-sign) (negative? current-sign)) -1]
    [(and (positive? current-sign) (negative? last-sign)) 1]
    [else 0]))

(define zero-crossing
  (stream-map sign-change-detector
              sense-data
              (stream-cons 0 sense-data)))
