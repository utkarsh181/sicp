#lang racket/base

;; Representation of table using mutable pairs.

(provide make-table
         lookup
         insert!)

(require sicp)

;; Return a table which used SAME-KEY? to identify similars keys.
(define (make-table same-key?)
  (let ([local-table (list '*table*)])
    (define (assoc key records)
      (cond
        [(null? records) #f]
        [(same-key? key (caar records))
         (car records)]
        [else (assoc key (cdr records))]))

    (define (lookup key)
      (let ([record (assoc key (cdr local-table))])
        (if (not record)
            #f
            (cdr record))))

    (define (insert! key value)
      (let ((record (assoc key (cdr local-table))))
        (if record
            (set-cdr! record value)
            (set-cdr! local-table
                      (cons (cons key value)
                            (cdr local-table))))
        (void)))

    (define (dispatch m)
      (cond
        [(eq? m 'lookup-proc) lookup]
        [(eq? m 'insert-proc) insert!]
        [else
         (raise-arguments-error 'make-table
                                "incorrect operation"
                                "op" m)]))
    dispatch))

;; Lookup for key KEY in table TABLE
(define (lookup key table)
  ((table 'lookup-proc) key))

;; Destructively insert KEY-VALUE pair in table TABLE
(define (insert! key value table)
  ((table 'insert-proc) key value))
