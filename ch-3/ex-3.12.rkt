#lang racket/base

(provide mlist
         mlast-pair
         append!)

;; Return a mutable list made up of ARGS
(define (mlist . args)
  (define (rec lst)
    (if (null? lst)
        '()
        (mcons (car lst) (rec (cdr lst)))))
  (rec args))

;; Return last-pair of mutable list X
(define (mlast-pair x)
  (if (null? (mcdr x))
      x
      (mlast-pair (mcdr x))))

;; Append mutable list X and Y
(define (append! x y)
  (set-mcdr! (mlast-pair x) y)
  x)

(define x (list 'a 'b))
(define y (list 'c 'd))
(define z (append x y))

(define mx (mlist 'a 'b))
(define my (mlist 'c 'd))
(define mz (append! mx my))
