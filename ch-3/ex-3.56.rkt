#lang racket

(provide stream-scale
         merge)

;; Return a stream by scaling stream S by factor of FACTOR.
(define (stream-scale s factor)
  (stream-map (lambda (x) (* x factor)) s))

;; Merge stream S1 and S2
(define (merge s1 s2)
  (cond
    ((stream-empty? s1) s2)
    ((stream-empty? s2) s1)
    (else
     (let ((s1-car (stream-first s1))
           (s2-car (stream-first s2)))
       (cond
         ((< s1-car s2-car)
          (stream-cons s1-car (merge (stream-rest s1) s2)))
         ((> s1-car s2-car)
          (stream-cons s2-car (merge s1 (stream-rest s2))))
         (else
          (stream-cons s1-car
                       (merge (stream-rest s1) (stream-rest s2)))))))))

(define S
  (stream-cons
   1
   (merge (merge (stream-scale S 2) (stream-scale S 3))
          (stream-scale S 5))))
