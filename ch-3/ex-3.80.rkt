#lang racket

(require "ex-3.50.rkt")
(require "ex-3.53.rkt")
(require "ex-3.56.rkt")
(require "ex-3.77.rkt")

;; TODO 2021-09-07: Learn how RLC circuits work.

;; Return a procedure that can be used to model RLC circuit where R, L and C are
;; resistance, inductance and capacitance.
(define (RLC R L C dt)
  ;; A procedure where iL0 and vC0 are intial value of {current, voltage}
  ;; passing through inductor L and capacitor C respectively.
  (lambda (iL0 vC0)
    (define iL (integral (stream-lazy diL) iL0 dt))
    (define vC (integral (stream-lazy dvC) vC0 dt))
    (define diL
      (stream-add (stream-scale vC (/ 1 L))
                  (stream-scale iL (/ (* -1 R) L))))
    (define dvC (stream-scale iL (/ -1 C)))
    ;; NOTE 2021-09-07: I was confused how to finalize the result but at last
    ;; was helped by Atupal at SICP Wiki:
    ;; <http://community.schemewiki.org/?sicp-ex-3.80>.
    (stream-map cons iL vC)))

(define RLC1 ((RLC 1 0.2 1 0.1) 0 10))
