#lang racket

(require "ex-3.50.rkt")

(define x
  (stream-map displayln (stream-range 0 10)))
