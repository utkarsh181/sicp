#+title: SICP Exercise 3.63
#+author: Utkarsh Singh

#+begin_src scheme
(define (sqrt-stream x)
  (cons-stream 1.0 (stream-map
		    (lambda (guess)
		      (sqrt-improve guess x))
		    (sqrt-stream x))))
#+end_src

Yes.  Alyssa is right.  If we used this procedure combined with
=(lambda () exp)=, like tree recursive Fibonacci process we would end
up with time complexity of O(2^n).  With memoization, we could save
some of the redundant computation, but due to recursion, each time we
are generating a new stream we are cut-shorting our memoization
efforts.
