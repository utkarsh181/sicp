#lang racket/base

(require sicp)
(require "ex-3.13.rkt")

;; Check if list LST contains a cycle, that is, whether a program that tried to
;; find the end of the list by taking successive cdrs would go into an infinite
;; loop.
(define (cycle? lst)
  (define (iter slow fast)
    (cond
      [(or (null? fast) (null? (cdr fast))) #f]
      [(eq? (car slow) (car fast)) #t]
      [else
       (iter (cdr slow) (cddr fast))]))
  (if (null? lst)
      #f
      (iter lst (cdr lst))))

;;; Testing

(define z1 (make-cycle '(a b c)))
(define z2 '(a b c))
(set-cdr! (cdr z2) z2)
