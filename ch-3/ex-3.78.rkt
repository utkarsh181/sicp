#lang racket

(require "ex-3.53.rkt")
(require "ex-3.56.rkt")
(require "ex-3.77.rkt")

;; TODO 2021-09-07: Learn how homogeneous second-order linera differential
;; equations work.

;; Solve equation f''(y) - a*f'(y) - by = 0, where Y0 and DY0 are intial values
;; of f and f', A and B are constants and DT is small value of increment.
(define (solve-2nd a b y0 dy0 dt)
  (define y (integral (stream-lazy dy) y0 dt))
  (define dy (integral (stream-lazy ddy) dy0 dt))
  (define ddy (stream-add (stream-scale dy a) (stream-scale y b)))
  y)
